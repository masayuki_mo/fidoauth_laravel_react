import { createSlice } from '@reduxjs/toolkit';

export interface loadingAnimationInterface {
    Animation?: {
        isLoading: boolean,
    }
}

const initialState = {
    isLoading: false,
}

const slice = createSlice({
    name: "Animation",
    initialState,
    reducers: {
        LOADING_ANIMATION: (state: any = false, action: any) => {
            return Object.assign({}, state,
                {
                    isLoading: action.isLoading,
                }
            )
        }
    }
});

export default slice.reducer;
export const {
    LOADING_ANIMATION
} = slice.actions;


