import { createSlice } from '@reduxjs/toolkit';

export interface toastrAnimationInterface {
    Toastr?: {
        toastrLoading: boolean,
        toastrText   : string,
        toastrMode   : string,
    },
    dispatch?: any
}

const initialState = {
    toastrLoading: false,
    toastrText   : '',
    toastrMode   : '',
}

const slice = createSlice({
    name: "Toastr",
    initialState,
    reducers: {
        TOASTR_ANIMATION: (state: any = false, action: any) => {
            return Object.assign({}, state,
                {
                    toastrLoading   : action.toastrLoading,
                    toastrText      : action.toastrText,
                    toastrMode      : action.toastrMode,
                }
            )
        }
    }
});

export default slice.reducer;
export const {
    TOASTR_ANIMATION
} = slice.actions;


