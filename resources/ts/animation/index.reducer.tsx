import loadingAnimation from './loading.animation.reducer';
import toastrAnimation from './toastr.animation.reducer';

export const animationReducers = {
    Animation   : loadingAnimation,
    Toastr      : toastrAnimation,
};


