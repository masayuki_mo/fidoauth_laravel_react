import { dateTime } from '../_lib/datetime/datetime.service';


export class CheckHelper
{
    private dF = new dateTime()

    /**
     * 現在時刻と引数の時刻の差分（分）が所定時間以上経過しているか判定
     * 
     * @param date1 Date
     * @param date2 Date
     * @param limit number [5]
     * @return boolean
     */
    public checkTimer(date: Date, limit: number = 0): boolean
    {
        const _diff = this.dF.diff('minute', date);
        return (_diff > limit || _diff < -(limit) || _diff == 0) ? true : false;
    }
}