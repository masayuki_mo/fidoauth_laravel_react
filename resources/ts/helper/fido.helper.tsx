import { initialStateServer, ServerInterface } from '../_lib/fido/fido.interface';
import FidoService from '../_lib/fido/fido.service';



export class FidoHelper
{
    private FS: FidoService;
    private PARAM: ServerInterface = initialStateServer;

    constructor()
    {
        this.FS = new FidoService();
    }

    /**
     * FidoServiceオブジェクトを返す
     * 
     * @return FidoService
     */
    public callFidoService(): FidoService
    {
        return this.FS;
    }

    /**
     * Fido認証開始
     * 
     * @param param ServerInterface
     * @return Promise<boolean> 
     */
    public async loginStart(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .startFidoLoginProcess();
    }

    /**
     * 認証用クライアントクレデンシャルをサーバーに返す
     * 
     * @return Promise<boolean>
     */
    public async loginResponce(): Promise<boolean>
    {
        return this.FS
                .responceFidoLoginProcess();
    }

    /**
     * FidoKey登録開始
     * 
     * @param param ServerInterface
     * @return Promise<boolean>
     */
    public async registStart(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .startFidoRegistProcess();
    }

    /**
     * FidoKey登録用クライアントクレデンシャルをサーバに返す
     * 
     * @return Promise<boolean>
     */
    public async registResponce(): Promise<boolean>
    {
        return this.FS
                .responceFidoRegistProcess();
    }

    /**
     * Fido認証済み情報を削除する
     * 
     * @param param ServerInterface
     * @return Promise<boolean>
     */
    public async logout(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .logoutStart();
    }

    /**
     * 登録済みのFidoKey情報を消す
     * 
     * @param param ServerInterface
     * @return Promise<boolean>
     */
    public async delete(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .deleteCredential();
    }

    /**
     * FidoKey登録済みか確認
     * 
     * @param param ServerInterface
     * @return Promise<boolean>
     */
    public async checkRegist(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .checkRegist();
    }

    /**
     * FidoKey認証済みか確認
     * 
     * @param param ServerInterface
     * @return Promise<boolean>
     */
    public async checkExpired(param: ServerInterface): Promise<boolean>
    {
        this.PARAM = param;
        return this.FS
                .setServerParam(this.PARAM)
                .checkExpired();
    }
}