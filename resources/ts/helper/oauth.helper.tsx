import OauthService from '../_lib/token/oauth.service';

export class OauthHelper
{
    private OS!: OauthService;

    private PARAM   : any = {};
    private JOB     : string = '';
    private PersonalTokens: any = {};

    public constructor(
        job: string,
        param: any 
    ) {
        this.PARAM   = param;
        this.JOB     = job;
        this.OS = new OauthService();
    }

    public setPersonalToken(tokens: any): this
    {
        this.PersonalTokens = tokens;
        return this;
    }

    public async run(): Promise<any>
    {
        return await this.callAPI();
    }

    
    private async callAPI(): Promise<any>
    {
        const personal = this.OS
                        // PersonalToken管理機能呼び出し
                        .callAccessTokenObj()
                        // APIコール用サーバーオプション設定
                        .setServerParam(this.PARAM);
        
        // PersonalTokenを新規発行させる
        if ( this.JOB === 'getPersonalTokens') {
            return await personal[this.JOB]()
                .then((result) => result.getTokens());
        
        // 発行済PersonalTokenを削除する
        } else if (this.JOB === 'deletePersonalToken') {
            // トークン情報を先にセット
            return await personal.setPersonalToken(this.PersonalTokens)
            // 有効期限切れのトークンを全削除
                .expirePersonalToken()
            // 削除後のトークン一覧を返す
                .then((result) => result.getTokens());
        
        } else if (this.JOB === 'expirePersonalToken') {
            // トークン情報を先にセット
            return await personal.setPersonalToken(this.PersonalTokens)
            // 有効期限切れのトークンを全削除
                .expirePersonalToken()
            // 削除後のトークン一覧を返す
                .then((result) => result.getTokens());
        }
    }


}