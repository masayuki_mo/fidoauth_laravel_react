import { TokenInterface, initialState as defToken } from '../reducers/Token';
import OauthService from '../_lib/token/oauth.service';
import TokenService from '../_lib/token/token.service';


/**
 * meta情報からaccess-tokenを取得する
 */
export function getAccessTokenFromHeader(): string
{
    const token:any = document.getElementsByName('access-token')[0]
    return ('content' in token) ? token.content : '';        
}

/**
 * meta情報からcsfr-tokenを取得する
 */
export function getCSRFTokenFromHeader(): string
{
    const token:any = document.getElementsByName('csrf-token')[0]
    return ('content' in token) ? token.content : '';
}

/**
 * 
 */
export function getCurrentURL(): string
{
    const url: any = document.getElementsByName('current')[0];
    return ('content' in url) ? url.content : '';
}

export class TokenHelper
{
    private SLS = new OauthService().callSaveLoadObj();
    private TO: TokenService = new OauthService().callTokenObj();
    // private TypeOfToken: TokenInterface = defToken;

    public async getBeareTokenFromHeader(): Promise<TokenInterface>
    {
        const _token = getAccessTokenFromHeader();
        if (_token === '') {
            return defToken;
        }
        if (!await this.setToken(_token).saveToken()){

        }
        return this.getToken();
    }

    /**
     * トークン情報をセット
     * TokenInterfaceのプロパティーと一致する内容のみ保持
     * 
     * @param token 
     */
    public setToken(token: any): TokenHelper
    {
        this.TO.setToken(token);
        return this;
    }

    /**
     * トークン情報をCookieに保管する
     * 同一Keyが存在する場合上書き
     * 
     * @return Promise<boolean>
     */
    public async saveToken(): Promise<boolean>
    {
        await this.TO.saveToken();
        if (this.TO.getStatusObj().checkError()) {
            return false;
        }
        return true;
    }

    /**
     * ヘルパーに保持されているトークンを返す
     * 
     * @return TokenInterface
     */
    public getToken(): TokenInterface
    {
        return this.TO.getToken();
    }

    /**
     * Cookieからトークン情報を取得
     * 存在しない場合はinitialオブジェクトを返す
     * 
     * @return Promise<TokenInterface>
     */
    public async loadToken(): Promise<TokenInterface>
    {
        const token = await this.TO.loadToken();
        this.setToken(token);
        return this.checkAccessToken() ? this.TO.getToken() : defToken;
    }

    /**
     * access_tokenが設定されているか確認
     * 
     * @return boolean
     */
    public checkAccessToken(): boolean
    {
        return (this.TO.callToken().getAccessToken() !== '') ? true : false;
    }

}