// import AjaxParseHelper from './ajax.parse.helper';
import AjaxService from '../_lib/http/ajax.service';

export class AjaxHelper
{
    private aj: AjaxService;

    private info: any = {};
    private error: any = {}

    public constructor(){
        this.aj = new AjaxService();
    }

    /**
     * APIを叩く
     * 
     * @param param 送信情報
     * @return Promise<object>
     */
    public async callAPI(param: any): Promise<object>
    {
        return this.callAjaxService(param);
    }

    /**
     * Ajaxサービスの呼び出し
     * 
     * @param param 
     * @return Promise<object>
     */
    private async callAjaxService(param: any): Promise<object>
    {
        return this.aj.setURL(param.url)
        .setMethod(param.method)
        .setBody(param.body)
        .setHeader(param.header)
        .buildRequestParam()
        .getResult();
    }


}
