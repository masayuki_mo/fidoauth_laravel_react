
import { createSlice } from '@reduxjs/toolkit';


export interface ResponceInterface {
  Responce?: {
    get_error: string;
    load_result: string;
    result_header: any;
    result_body: any;
  }
}

export const initialState = {
  get_error: '',
  load_result: '',
  result_header: {
    headers: 'Not Found',
  },
  result_body: {
    bodys: 'Not Found',
  },
}

const slice = createSlice({
  name: "Responce",
  initialState,
  reducers: {
    GET_ERROR: (state: any, action: any) => {
      return action.hasError;
    },
    LOAD_RESULT: (state: any, action: any) => {
      return action.isLoading;
    },
    FETCH_SUCCESS: (state: any, action: any) => {
      return Object.assign({}, state,
        {
            result_header: action.comments.header,
            result_body: action.comments.data,
        }
      )
    }
  }
});

export default slice.reducer;
export const { GET_ERROR, LOAD_RESULT, FETCH_SUCCESS } = slice.actions;


