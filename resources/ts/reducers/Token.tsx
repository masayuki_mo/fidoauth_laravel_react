import { createSlice } from '@reduxjs/toolkit';

export interface TokenPropsInterface
{
    Token: {
        access_token    : string,
        refresh_token   : string,
        expires_in      : number,
        token_type      : string,
    },
    PersonalToken: {
        [key: number]: PersonalToken
    }
    Depogit: {
        error_counter   : number,
        last_regist     : string,
    },
    OAuth: {
        api             : {
            grant_token : { path: 'oauth/token' , method: 'POST' }
        }
    }
}

export interface TokenInterface
{
    access_token    : string,
    refresh_token   : string,
    expires_in      : number,
    token_type      : string,
    [key: string]   : string | number,
}

export interface PersonalToken
{
    client: {
        created_at  : string,
        id          : string,
        name        : string,
        password_client : boolean,
        personal_access_client: boolean,
        provider    : string | null,
        redirect    : string,
        revoked     : boolean,
        updated_at  : string,
    },
    client_id       : string,
    created_at      : string,
    expires_at      : string,
    id              : string,
    name            : string,
    revoked         : boolean,
    scopes          : object,
    updated_at      : string,
    user_id         : number,
}

export interface TokenDepogitInterface
{
    error_counter   : number,
    last_regist     : string,    
}

export const initialState = {
    access_token    : '',
    refresh_token   : '',
    expires_in      : 1800,
    token_type      : '',
}

const slice = createSlice({
    name: 'Token',
    initialState,
    reducers: {
        SetPersonalTokens: (state: any, action: any) => {
            return Object.assign(
                {},
                state,
                {
                    personalToken: {
                        ...state.PersonalToken,
                        ...action.personalToken
                    }
                }
            )
        },
        SetToken: (state: any, action: any) => {

            // console.log(action);
            return Object.assign({}, state,
                {
                    access_token    : action.access_token,
                    refresh_token   : action.refresh_token,
                    expires_in      : action.expires_in,
                    token_type      : action.token_type,
                }
            )
        },
        UpdateAccessToken: (state: any, action: any) => {

            // console.log(action);
            return Object.assign({}, state,
                {
                    access_token    : action.access_token,
                }
            )
        },
        UpdateExpires: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    expires_in         : action.expires_in,
                }
            )
        },

    }
});

export default slice.reducer;
export const {
    SetPersonalTokens,
    SetToken,
    UpdateAccessToken,
    UpdateExpires,
} = slice.actions;