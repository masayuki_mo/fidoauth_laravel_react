import Fido     from './Fido';
import Form     from './Form';
import Responce from './Responce';
import Token    from './Token';
import Page     from './Page';

import { animationReducers } from '../animation/index.reducer';

export const reducer = {
    Form    : Form,
    Fido    : Fido,
    Responce: Responce,
    Token   : Token,
    Page    : Page,
    ...animationReducers
}



