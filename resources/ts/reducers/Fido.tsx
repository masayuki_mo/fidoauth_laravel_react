import { createSlice } from '@reduxjs/toolkit';

export interface FidoPropsInterface
{
    Fido?                               : any;
    publicKeyCredentialCreationOptions? : any;
    registrationId?                     : any;
    publicKeyCredentialRequestOptions?  : any;
    assertionId?                        : any;
    credentialResponse?                 : any;
    requestPath?                        : any;
    fidoJob?                            : string;
    dispatch?                           : any;
}

export const initialState = {
    Fido: {},
    requestPath: {
        expiresOut      : 'api/expire',
        bearerGet       : 'oauth/token',
        parsonalToken   : 'oauth/tokens',
        registStart     : 'api/registration/start',
        registFinish    : 'api/registration/finish',
        loginStart      : 'api/assertion/start',
        loginFinish     : 'api/assertion/finish',
    },
    fidoJob: 'registStart'
}

const slice = createSlice({
    name: 'Fido',
    initialState,
    reducers: {
        SetFidoRegistState: (state: any, action: any) => {

            // console.log(action);
            return Object.assign({}, state,
                {
                    publicKeyCredentialCreationOptions: action.publicKeyCredentialCreationOptions,
                    registrationId:  action.registrationId,
                }
            )
        },
        SetFidoLoginState: (state: any, action: any) => {

            // console.log(action);
            return Object.assign({}, state,
                {
                    publicKeyCredentialRequestOptions: action.publicKeyCredentialRequestOptions,
                    assertionId:  action.assertionId,
                }
            )
        },
        updateResponse: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    credentialResponse: action.credentialResponse,
                }
            )
        },
        updateJOB: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    fidoJob: action.fidoJob,
                }
            )
        },
        resetFido: (state: any, action: any) => {
            return Object.assign({}, state, initialState)
        }
    }
});

export default slice.reducer;
export const {
    SetFidoRegistState,
    SetFidoLoginState,
    updateResponse,
} = slice.actions;