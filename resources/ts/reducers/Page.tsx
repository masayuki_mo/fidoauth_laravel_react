
import { createSlice } from '@reduxjs/toolkit';

export interface PagePropsInterface {
  Page?: {
    page    : string
    expired : boolean
    cleaner : Date
  },
  dispatch? : any;
}

export const initialState = {
  page    : 'login',
  expired : false,
  cleaner : new Date(),
}

const slice = createSlice({
  name: "Page",
  initialState,
  reducers: {
    // 表示ページを切り替え
    MOVE_PAGE: (state: any, action: any) => {
      console.log(action);
      return Object.assign(
        {}, state,
        {
          page : action.page
        }
      )
    },
    // 認証済み切り替え
    EXPIRED: (state: any, action: any) => {
      return Object.assign(
        {}, state,
        {
          expired : action.expired
        }
      )
    },
    // 日付更新
    SET_CLEANER: (state: any, action: any) => {
      return Object.assign(
        {}, state,
        {
          cleaner : action.cleaner
        }
      )
    },
  }
});

export default slice.reducer;
export const {
  MOVE_PAGE, EXPIRED
} = slice.actions;

