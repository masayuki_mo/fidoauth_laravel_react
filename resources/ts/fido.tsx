require('./bootstrap');

import React from 'react';
import { Provider } from 'react-redux'
import { render } from 'react-dom';

// import './index.css';

// import 'bootstrap/dist/css/bootstrap.css';
import TwoFA from './component/TwoFA';

import LoadingAnimation from './animation/loading.animation';
import ToastrAnimation from './animation/toastr.animation';

// import rootReducer from './reducers'
import { createStore } from './store/configureStore';

const store = createStore();

interface AppPropsInterface {
  dispatch?: any;
}

export default class App
  extends React.Component <AppPropsInterface, {}>
{
  render() {
    return (
      <Provider store={ store }>
        <TwoFA />
        <LoadingAnimation />
        <ToastrAnimation />
      </Provider>
    )
  }
}
render (
    <App />,
    document.getElementById('regist')
);