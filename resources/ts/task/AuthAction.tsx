import { put, call, takeEvery, select } from 'redux-saga/effects';
import { AnimationTask, loading_animation, toastr_animation } from '../animation/index.task';

import { FormPropsInterface } from '../reducers/Form';
import { FidoPropsInterface } from '../reducers/Fido';
import { TokenInterface, TokenPropsInterface } from '../reducers/Token';

// import { TokenChecker, TokenSaver } from './TokenAction';
import { TokenSaver } from './TokenAction';
import {
    TokenHelper, getAccessTokenFromHeader, getCSRFTokenFromHeader
} from '../helper/token.helper';
import { initialStateToken } from '../_lib/token/oauth.interface';

const paramForm  = (state: FormPropsInterface) => state.Form;
const paramFido  = (state: FidoPropsInterface) => state.Fido;
const paramToken = (state: TokenPropsInterface) => state.Token;
const _form = {};
const _fido = {};



/**
 * FIDO作成した認証情報を返す
 */
export function* initialTokenCheck()
{
    yield takeEvery(
        'AuthAction/All_Token_Check', allTokenCheck
    );
}

export function* fidoExpireCheck()
{
    yield takeEvery(
        'RequestAction/Fido_Expire_Check', checkFidoCirtificate
    );
}


export function* allTokenCheck()
{
    const TH = new TokenHelper();
    // Headerにトークンが設定されているか確認
    const token: TokenInterface = yield TH.getBeareTokenFromHeader();
    if (token != initialStateToken) {

    }
    
}

/**
 * Fido認証済みかを問い合わせる
 */
export function* checkFidoCirtificate() {
    const form = yield select(paramForm);
    const fido = yield select(paramFido);
}

/**
 * Bearerトークンが未取得の場合新規に取得する
 * @param form 
 * @param fido 
 * @return boolean
 * @throw Form/updateBearerToken
 */
export function* checkBearerToken(): any
{
    let token: TokenInterface;

    // トークンがローカルに保存済みかチェック
    // HTMLヘッダーに記載している場合は保存する意味がないのでコメント
    // const check = yield call(TokenChecker);
    const check = false;

    if (!check) {
        // トークンが存在しない場合は
        // トークンの取得をコール
        // token =  yield call( getBearerToken );

        // return token ? true : false;
    } else {
        token = yield select(paramToken);
        yield put({
            type: 'Form/updateBearerToken',
            bearer: token.access_token
        });

        return true;
    }
}



/**
 * リクエスとオプションの生成
 * @param param any
 * @param path string
 * @return object 
 */
const buildParam = (param: any, path: string): object => {
    const header = [];
    let body = {};

    if (param.bearer !== "") {
        const token: string = getCSRFTokenFromHeader();
        header.push(
            'X-CSRF-TOKEN: ' + token,
            'Authorization: Bearer ' + param.bearer
        );
    }
    return {
        url: param.url + path,
        method: param.method,
        header: header.concat(param.header),
        body: {...body, ...param.body},
        bearer: param.bearer,
    }
}
