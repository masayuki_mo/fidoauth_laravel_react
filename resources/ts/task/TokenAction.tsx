import { call, put, takeEvery, select } from 'redux-saga/effects';
import { toastr_animation } from '../animation/index.task';

import { 
    TokenHelper,
    getAccessTokenFromHeader, getCSRFTokenFromHeader
} from '../helper/token.helper';
import { OauthHelper } from '../helper/oauth.helper';

import { TokenPropsInterface } from '../reducers/Token';
import { FormPropsInterface } from '../reducers/Form';

const taToken = (state: TokenPropsInterface) => state.Token;
const taForm = (state: FormPropsInterface) => state.Form;
const taDepogit = (state: TokenPropsInterface) => state.Depogit;

/**
 * トークンの存在確認
 * Cookie、Localstrageいずれかに存在する場合は
 * トークン情報をSTATEに保存
 */
export function* checkToken()
{
    yield takeEvery(
        'TokenAction/Check_Token', TokenChecker
    );
}

/**
 * トークンの存在確認
 * Cookie、Localstrageいずれかに存在する場合は
 * トークン情報をSTATEに保存
 */
export function* saveToken()
{
    yield takeEvery(
        'TokenAction/Save_Token', TokenSaver
    );
}

export function* getPersonalTokens()
{
    yield takeEvery(
        'TokenAction/Get_PersonalTokens',
        doFetch,
        'getPersonalTokens'
    );
}

export function* deletePersonalToken()
{
    yield takeEvery(
        'TokenAction/Delete_PersonalTokens',
        doFetch,
        'deletePersonalToken'
    );
}

export function* expirePersonalToken()
{
    yield takeEvery(
        'TokenAction/Expire_PersonalTokens',
        doFetch,
        'expirePersonalToken'
    );
}


/**
 * トークンをStoreにセットする
 * 
 * @param token 
 */
export function* SetToken(token: any)
{
    yield put({
        type            : 'Token/SetToken',
        access_token    : token.access_token,
        refresh_token   : token.refresh_token,
        expires_in      : token.expires_in,
        token_type      : token.token_type,
    });
}

/**
 * トークン情報がCookie、LocalStrage
 * どちらかに存在するか確認
 * 存在する場合Propsに保持
 */
export function* TokenChecker()
{
    console.log('Start Check Token');
    const TH = new TokenHelper();
    yield TH.loadToken();

    if (yield TH.checkAccessToken()) {
        yield SetToken(TH.getToken());
        return true;

    } else {
        // yield put({ type: 'Bearer_Token' });
        return false;
    }
}

/**
 * トークンをCookieとローカルストレージに保存
 */
export function* TokenSaver()
{
    console.log('Start Save Token');
    const TH = new TokenHelper();
    const token = yield select(taToken);
    const result = yield TH.setToken(token).saveToken();

    if (!result) {
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Token Save Error',
            toastrMode    : 'error',
        });
    }
}

function* doFetch(job: string)
{
    console.log('Start :: ' + job);

    // Oauthヘルパーを作成
    const oauth = new OauthHelper(
        job,
        {
            ...yield select(taForm),
            bearer: yield getAccessTokenFromHeader(),
            csrf: yield getCSRFTokenFromHeader(),
        }
    );

    if (job == 'getPersonalTokens') {
        const result = yield oauth.run();
        yield put({
            type: 'Token/SetPersonalTokens',
            personalToken: result.message
        });
        yield put({
            type: 'TokenAction/Expire_PersonalTokens'
        });
    } else if (job == 'deletePersonalToken') {
        const token = yield select(taToken);
        const result = yield oauth
                            .setPersonalToken(token.personalToken)
                            .run();
        console.log(result);
    } else if (job == 'expirePersonalToken') {
        console.log('Why Japanese Peaple');
        const token = yield select(taToken);
        const result = yield oauth
                            .setPersonalToken(token.personalToken)
                            .run();
        console.log(result);
    }
}
