import { put, call, takeEvery, select } from 'redux-saga/effects';

import { PagePropsInterface } from '../reducers/Page';
import { CheckHelper } from '../helper/check.helper';

const caPage  = (state: PagePropsInterface) => state.Page;


export function* getParsonalTokens()
{
    yield takeEvery(
        'CheckAction/CALL_CLEANER', callCleaner
    );
}

/**
 * APIコール処理
 * APIパスにてアクセス先の切り替えと呼び出し後の投機先を変える
 * 
 * @param job string APIパス
 */
export function* callCleaner() {
    const CH = new CheckHelper();

    // 前回のクリーニング時間を取得しチェッカーにかける
    const page = yield select(caPage);
    if (yield CH.checkTimer(page.cleaner, 3600)) {
        // Oauthトークンのゴミを消す
        yield put({
            type: 'TokenAction/Get_PersonalTokens'
        });
    }

    // 3600秒後に再チェック
    /**setInterval(
        callCheckActionCleaner, 3600
    );*/
}

function* callCheckActionCleaner()
{
    yield put({
        type: 'CheckAction/Call_CLEANER'
    });
}
