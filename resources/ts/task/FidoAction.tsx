import { put, call, takeEvery, select } from 'redux-saga/effects';
import {
    FidoHelper
} from '../helper/fido.helper';
import { 
    getAccessTokenFromHeader, getCSRFTokenFromHeader, getCurrentURL
} from '../helper/token.helper';
import { loading_animation, toastr_animation } from '../animation/index.task';
import { FidoPropsInterface } from '../reducers/Fido';
import FidoService from '../_lib/fido/fido.service';
import { ServerInterface } from '../_lib/token/oauth.interface';
import { FormPropsInterface } from '../reducers/Form';

const faFido = (state: FidoPropsInterface) => state.Fido;
const faForm  = (state: FormPropsInterface) => state.Form;
/**
 * ユーザー作成処理、FIDO認証キー取得開始
 */
export function* registStart()
{
    yield takeEvery( 'FidoAction/Regist_Start', RegistStart );
}

/**
 * ログイン処理、FIDO認証キー取得開始
 */
export function* loginStart()
{
    yield takeEvery( 'FidoAction/Login_Start', LoginStart );
}

/**
 * ログイン処理、FIDO認証キー取得開始
 */
export function* logoutStart()
{
    yield takeEvery( 'FidoAction/Logout_Start', LogoutStart );
}

/**
 * Fido認証キーの削除
 */
export function* deleteCredntial()
{
    yield takeEvery( 'FidoAction/Credential_Delete', DeleteCredential );
}

/**
 * 認証期限確認
 */
export function* expiredCredential()
{
    yield takeEvery( 'FidoAction/Check_Expired', ExpiredCredential);
}

/**
 * FidoKey登録済み確認
 */
export function* alreadyCertified()
{
    yield takeEvery( 'FidoAction/Check_Certified', AlreadyCertified);
}

/**
 * ユーザー作成処理
 * @dispatch -> Fido/updateResponse
 * @dispatch -> Form/updateBody
 * @dispatch -> Fido_Regist_Finish
 */
export function* RegistStart() {
    // 読込中表示
    yield call(loading_animation, true);

    // Fido要求用オプションを読み込み
    const param: ServerInterface = yield buildServerParam()
    const FH:FidoHelper = new FidoHelper();

    const build = yield FH.registStart(param);
    if (!build) {
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Can`t Build ClientCredential',
            toastrMode    : 'error',
        })
        return;
    }
    yield call(loading_animation, false);

    // クレデンシャルを返す
    yield call(loading_animation, true);
    const responce = yield FH.registResponce();
    if (!responce){
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Credentail Server Responce',
            toastrMode    : 'error',
        })
        return;
    }
    // ページ表示ステータス更新
    yield put({type: 'FidoAction/Check_Certified'});

    // 登録完了後の処理が入る
    // 読込中消す
    yield call(loading_animation, false);
    yield call(toastr_animation, {
        toastrLoading : true,
        toastrText    : 'Login Success',
        toastrMode    : 'success',
    });
}


/**
 * ログイン処理を行いクレデンシャル情報を返す
 */
export function* LoginStart() {
    // 読込中表示
    yield call(loading_animation, true);

    
    // Fido要求用オプションを読み込み
    const param = yield buildServerParam();
    const FH:FidoHelper = new FidoHelper();
    
    // ClientCredentialを受け取る
    const login = yield FH.loginStart(param);
    if (!login) {
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Can`t Build ClientCredential',
            toastrMode    : 'error',
        });
        return;
    }
    yield call(loading_animation, false);

    // クレデンシャルを返す
    yield call(loading_animation, true);
    const responce = yield FH.loginResponce();
    if (!responce ){
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Credentail Server Responce',
            toastrMode    : 'error',
        });
        return;
    }

    // 登録完了後の処理が入る
    // 読込中消す
    yield call(loading_animation, false);
    yield call(toastr_animation, {
        toastrLoading : true,
        toastrText    : 'Login Success',
        toastrMode    : 'success',
    });

    // 転送されてきている場合元ページに戻す
    if (param.current !== '') {
        window.location.href = param.current;
    }
}

/**
 * ログアウト処理を行う
 */
export function* LogoutStart() {
    // 読込中表示
    yield call(loading_animation, true);
    
    // Fido要求用オプションを読み込み
    const param = yield buildServerParam();
    const FH:FidoHelper = new FidoHelper();
    
    const del = yield FH.logout(param);
    if (!del) {
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Can`t Build ClientCredential',
            toastrMode    : 'error',
        })
        return;
    }

    // ログアウト完了後の処理が入る
    // 読込中消す
    yield call(loading_animation, false);
    yield call(toastr_animation, {
        toastrLoading : true,
        toastrText    : 'Logout Success',
        toastrMode    : 'success',
    });
}

/**
 * 登録済みのFidoKeyを削除する
 */
export function* DeleteCredential()
{
    // 読込中表示
    yield call(loading_animation, true);
    
    // Fido要求用オプションを読み込み
    const param = yield buildServerParam();
    const FH:FidoHelper = new FidoHelper();
    
    const del = yield FH.delete(param);
    if (!del) {
        // 読込中消す
        yield call(loading_animation, false);
        yield call(toastr_animation, {
            toastrLoading : true,
            toastrText    : 'Error Can`t Delete ClientCredential',
            toastrMode    : 'error',
        })
        return;
    }

    // ページ表示ステータス更新
    yield put({type: 'FidoAction/Check_Certified'});

    // 削除完了後の処理が入る
    // 読込中消す
    yield call(loading_animation, false);
    yield call(toastr_animation, {
        toastrLoading : true,
        toastrText    : 'Delete Success',
        toastrMode    : 'success',
    });
}

export function* AlreadyCertified()
{
    // Fido要求用オプションを読み込み
    const param = yield buildServerParam();
    const FH:FidoHelper = new FidoHelper();
    
    const ar = yield FH.checkRegist(param);

    // 表示ページをStateに登録
    if (ar) {
        yield put({type: 'Page/MOVE_PAGE', page: 'login'});
    } else {
        yield put({type: 'Page/MOVE_PAGE', page: 'regist'});
    }
}

export function* ExpiredCredential()
{
    
    // Fido要求用オプションを読み込み
    const param = yield buildServerParam();
    const FH:FidoHelper = new FidoHelper();
    
    const ex = yield FH.checkExpired(param);

    // 有効期限の可否をStateに登録
    if (ex) {
        yield put({type: 'Page/EXPIRED', expired: true});
    } else {
        yield put({type: 'Page/EXPIRED', expired: false});
    }
}

function* buildServerParam()
{
    const param: ServerInterface = yield select(faForm);
    param.bearer = yield getAccessTokenFromHeader();
    param.csrf = yield getCSRFTokenFromHeader();
    param.current = yield getCurrentURL();
    return param;
}

