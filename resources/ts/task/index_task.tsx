import { all } from 'redux-saga/effects';

// Load RequestAction
import {
    getParsonalTokens,
} from './CheckAction';

// Load FidoAction
import {
    registStart,
    loginStart, logoutStart,
    deleteCredntial,
    expiredCredential, alreadyCertified,
} from './FidoAction';

// Load TokenAction
import {
    checkToken,
    saveToken,
    getPersonalTokens,
    deletePersonalToken,
    expirePersonalToken,
} from './TokenAction';

// Load AnimationAction
import { AnimationTask } from '../animation/index.task';

export default function* rootSaga() {
    yield all([
        //TokenAction
        checkToken(), saveToken(),
        getPersonalTokens(),
        deletePersonalToken(),
        expirePersonalToken(),
        // CheckAction
        getParsonalTokens(),
        // FidoAction
        registStart(),
        loginStart(), logoutStart(),
        deleteCredntial(), expiredCredential(),
        alreadyCertified(),
        // ANimationActions
        ...AnimationTask,
    ]);
}