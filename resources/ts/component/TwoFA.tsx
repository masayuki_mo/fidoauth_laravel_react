import React from 'react';
import { connect } from 'react-redux';
import { PagePropsInterface, initialState } from '../reducers/Page';
import FidoLogin from './FidoLogin';
import FidoRegist from './FidoRegist';

const mapStateToProps = (state: any) => {
    return state;
}

export class TwoFA
    extends React.Component<PagePropsInterface, {}>
{
    constructor(props: any)
    {
        super(props);
        this.props.dispatch({type: 'FidoAction/Check_Certified'});
        this.props.dispatch({type: 'CheckAction/CALL_CLEANER'});
    }
    render() {
        const p = (this.props.Page) ? this.props.Page : initialState;
        const _figo = (p.page === 'regist') ? <FidoRegist /> : <FidoLogin />;
        return (
            <div className="container">
                <div className="container" id="regist">
                    <div className="tab-pane fade show active"
                        id="item1" role="tabpanel"
                        aria-labelledby="item1-tab">
                        
                        <div className="container">
                            <br></br>
                            <br></br>
                            { _figo }
                            <br></br>
                            <h6>認証機の用意をしてからボタンを押してください</h6>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps
)(TwoFA)
