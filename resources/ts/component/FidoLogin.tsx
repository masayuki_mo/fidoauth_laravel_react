import React from 'react';
import { connect } from 'react-redux';
import { PagePropsInterface, initialState } from '../reducers/Page';

const mapStateToProps = (state: any) => {
    return state;
}

export class FidoLogin
    extends React.Component<PagePropsInterface, {}>
{
    render() {
        const p = (this.props.Page) ? this.props.Page : initialState;
        return (
            <div className="container">
                <div className="col-sm-8">
                    <button
                        className="btn btn-primary mb-2"
                        onClick={
                            () => this.props.dispatch({ type: 'FidoAction/Login_Start'})
                        }>ログイン</button>
                </div>
                <br></br>
                <div className="col-sm-8">
                    <button
                        className="btn btn-primary mb-2"
                        onClick={
                            () => this.props.dispatch({ type: 'FidoAction/Logout_Start'})
                        }>ログアウト</button>
                </div>
                <br></br>
                <div className="col-sm-8">
                    <button
                        className="btn btn-primary mb-2"
                        onClick={
                            () => this.props.dispatch({ type: 'FidoAction/Credential_Delete'})
                        }>削除</button>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps
)(FidoLogin)