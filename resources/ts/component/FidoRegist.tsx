import React from 'react';
import { connect } from 'react-redux';
import { FormPropsInterface, initialState } from '../reducers/Form';

const mapStateToProps = (state: any) => {
    return state;
}

export class FidoRegist
    extends React.Component<FormPropsInterface, {}>
{
    render() {
        const r = (this.props.Form) ? 
                        this.props.Form : initialState;
        return (
            <div className="container">
                <div className="col-sm-8">
                    <button
                        className="btn btn-primary mb-2"
                        onClick={
                            () => this.props.dispatch({ type: 'FidoAction/Regist_Start'})
                        }>登録</button>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps
)(FidoRegist)