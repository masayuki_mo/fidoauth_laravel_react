import {
    // Interface読み込み
    TokenInterface, GrantInterface,
    // イニシャルステートの読み込み
    initialStateGrant, initialStateToken,
} from './oauth.interface';

import AccessTokenService from './access_token.service';
import CallClientsService from './call_clients.service';
import TokenService from './token.service';
import SaveLoadService from './save_load.service';

export default class OauthService
{
    public constructor() {}

    /**
     * PersonalTokenServiceを返す
     * 
     * @return AccessTokenService
     */
    public callAccessTokenObj(): AccessTokenService
    {
        return new AccessTokenService();
    }

    /**
     * ClientServiceを返す
     * 
     * @return CallClientsService
     */
    public callClientsTokenObj(): CallClientsService
    {
        return new CallClientsService();
    }

    /**
     * TokenServiceを返す
     * 
     * @return TokenService
     */
    public callTokenObj(): TokenService
    {
        return new TokenService();
    }

    /**
     * SaveLoadSerivceを返す
     */
    public callSaveLoadObj(): SaveLoadService
    {
        return new SaveLoadService();
    }

    public saveToken()
    {

    }

}