import { Param } from './config.service';

/**
 * APIコールオプションを作成
 * @param param 
 */
export function buildParam(
    param: Param
): object {
    const header = [];
    const p = param.getParam();
    header.push(
        'X-CSRF-TOKEN: ' + p.csrf,
        'Authorization: Bearer ' + p.bearer
    );

    return {
        url     : p.url,
        method  : p.method,
        header  : header.concat(p.header),
        body    : p.body,
        bearer  : p.bearer,
        csrf    : p.csrf,
    }
}

/**
 * パスワードグラントトークンの
 * APIコールオプションを作成
 * @param param 
 * @param csrf 
 * @param oa 
 */
export function buildParamGrantToken(
    param: any, csrf: string, oa: any
): object {
    const header = [];
    let body = {};

    header.push(
        'X-CSRF-TOKEN: ' + csrf
    );
    body = {
        ...body,
        ...{
            'grant_type': 'password',
            'client_id': '',
            'client_secret': '',
            'username': '',
            'password': '',
            'scope': '*',
        }
    };


    return {
        url: param.url ,
        method: param.method,
        header: header.concat(param.header),
        body: {...body, ...param.body},
        bearer: param.bearer,
    }
}


/**
 * トークンの有効期限確認
 * 
 * @param token トークンデータ
 * @param extime number デフォルト1800000（30分）
 * @return boolean 有効：true、無効：false
 */
export function checkExpired(token: any, extime: number = 1800000): boolean {
    const chTime = new Date(token.created_at).getTime();
    const now = new Date().getTime();
            
    if (( now - chTime) >= extime ) {
        return false;
    } else {
        return true;
    }
}
