import AjaxHelper from '../http/ajax.service';
import {
    // Interface読み込み
    AccessTokenInterface, initialStateAccessToken,
} from './oauth.interface';
import { Param, Paths } from './config.service';
import { buildParam, checkExpired } from './helper.service';

/**
 * AccessToken管理
 * 
 * サーバー保管されているAccessTokenを取得
 * 有効期限の確認、トークン削除、スコープの確認などを行う
 */
export default class AccessTokenService
{
    private aj: AjaxHelper;
    private PARAM: Param;
    private AccessTokens: Array<AccessTokenInterface> = [];
    private _TokenCache: Array<AccessTokenInterface> = [];
    private _TokenIndex: Array<string> = [];

    public constructor()
    {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
    }

    /**
     * API呼び出し用サーバーオプションの設定
     * 
     * @param data 
     * @return AccessTokenSetvice
     */
    public setServerParam(data: any): AccessTokenService
    {
        this.PARAM.setParam(data);
        return this;
    }

    /**
     * トークン情報を設定
     * 
     * @param tokens 
     * @return AccessTokenSetvice
     */
    public setPersonalToken(
        tokens: Array<AccessTokenInterface>
    ): AccessTokenService {

        this.AccessTokens = tokens;
        return this;
    }

    /**
     * AccessTokenの全て返す
     * 未取得、取得情報が０の場合空の配列が返る
     * 
     * @return Array<AccessTokenInterface> アクセストークン配列
     */
    public getTokens(): Array<AccessTokenInterface>
    {
        return this.AccessTokens;
    }

    /**
     * 作成済みのパーソナルトークンをすべてサーバーから取得
     */
    public async getPersonalTokens(): Promise<AccessTokenService>
    {
        const p:{[key:string]: any} = buildParam(this.PARAM);
        const op = Paths['getPersonalToken'];
        
        this.AccessTokens = await
            this.aj
                .setURL(p.url + op.path)
                .setMethod(op.method)
                .setHeader(p.header)
                .buildRequestParam()
                .getResult();
        return this;
    }

    /**
     * パーソナルトークンの削除
     */
    public async deletePersonalToken(id: string): Promise<any>
    {
        const p:{[key:string]: any} = buildParam(this.PARAM);
        const op = Paths['deletePersonalToken'];
        const url = p.url + op.path + '/' + id;
        
        await this.aj.setURL(url)
            .setMethod(op.method)
            .setHeader(p.header)
            .buildRequestParam()
            .getResult();
        
        console.log(this.checkDelete());
        this.aj.resetResult();
        this.aj.resetRequest();

        return this;
    }

    /**
     * 発行から30分を経過したトークンの削除
     * 
     * @return Promise<any>
     */
    public async expirePersonalToken(): Promise<any>
    {
        if (this.AccessTokens === []) {
            await this.getPersonalTokens();
        }
        // Tokenのインデックスを作成
        this.buildTokenIndexs();
        // 有効期限チェック開始
        await this.checkExpire();
        return this;
    }

    /**
     * Deleteメソッドの実行結果が「204」になっているか確認
     * 
     * @return boolean
     */
    private checkDelete(): boolean
    {
        const head = this.aj.callAjaxParseService().getResponce();
        if (head.status === 204) {
            return true;
        }
        return false;
    }

    /**
     * 有効期限切れのトークンを全て削除し
     * トークン配列から除外する
     * 
     * @return Promise<CallPersonalSerice>
     */
    private async checkExpire(): Promise<AccessTokenService>
    {
        const token = await this.popToken();

        if (token === initialStateAccessToken) {
            return this;
        }

        if (!checkExpired(token)) {
            console.log('delete personal token :: ' + token.id);
            await this.deletePersonalToken(token.id);
        } else {
            this._TokenCache.push(token);
        }

        if (Object.keys(this.AccessTokens).length > 0) {
            this.checkExpire();
        } else {
            this.updateTokenCache();
        }

        return this;
    }

    /**
     * トークン配列の最後からトークンを1件返す
     * 
     * @return Promise<AccessTokenInterface>
     */
    private async popToken(): Promise<AccessTokenInterface>
    {
        const number = Number(this._TokenIndex.pop());
        if (this._TokenIndex.length === 0) {
            return initialStateAccessToken;
        }
        const _token = this.AccessTokens[number];
        // delete this.PersonalToken[number];
        // console.log(Object.getOwnPropertyDescriptors(this.PersonalToken));
        
        return  (number === undefined) ? initialStateAccessToken : _token;
    }

    /**
     * Tokenオブジェクトから配列用のIndex配列を作成する
     * 
     * @return AccessTokenService
     */
    private buildTokenIndexs(): AccessTokenService
    {
        const indexes = Object.keys(this.AccessTokens);
        this._TokenIndex = (indexes.length > 0)? indexes : [];
        return this;
    }

    /**
     * トークン配列キャッシュをトークン配列に代入
     * 
     * @return AccessTokenService
     */
    private updateTokenCache(): AccessTokenService
    {
        this.AccessTokens = this._TokenCache;
        this._TokenCache = [];
        console.log(this.AccessTokens);
        return this;
    }
}