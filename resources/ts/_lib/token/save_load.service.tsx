import { CookieService } from '../cookie/cookie.service';
import { LocalStrageService } from '../strage/localstrage.service';
import {
    initialStateToken, 
    TokenInterface, ClientsInterface,
} from './oauth.interface';
import { Token } from './token';
import { TokenIndex } from './config.service';
import StateusService from '../status/status.service';

export default class SaveLoadService
{
    private SS: StateusService = new StateusService();

    private CS: CookieService;
    private LS: LocalStrageService;
    private TokenIndex = TokenIndex;
    private Token: Token = new Token;

    public constructor()
    {
        this.CS = new CookieService();
        this.LS = new LocalStrageService();
    }
    /**
     * 成否ステータス管理オブジェクトを返す
     * 
     * @return StatusService
     */
    public getStatusObj(): StateusService
    {
        return this.SS;
    }

    /**
     * トークンデータを設定
     * 設定時に、文字列への変換も同時に行う
     * 
     * @param token TokenInterface
     * @return SaveLoadService
     */
    public setToken(token: Token): SaveLoadService
    {
        if (!token.checkDefault()) {
            this.Token.setToken(token.getToken());
        }
        return this;
    }
    /**
     * トークンをLocalstrageとCookieに保存
     * どちらも失敗した場合のみエラーを記録
     * @param token 
     */
    public async saveToken(): Promise<SaveLoadService> {
        if (
            ! await this.saveCoockie(this.TokenIndex.token.key)
            || ! await this.saveLocalStrage(this.TokenIndex.token.key)
        ) {
            this.SS.setError(
                'Token Save Error',
                'SaveLoadService Save Process Error'
            );
        }

        return this;
    }

    /**
     * PersonalTokenをLocalstrageとCookieに保存
     * @param token 
     */
    public async savePersonalToken(
        token: TokenInterface
    ): Promise<SaveLoadService> {
        if (
            ! await this.saveCoockie(this.TokenIndex.token.key)
            || ! await this.saveLocalStrage(this.TokenIndex.token.key)
        ) {
            this.SS.setError(
                'Token Save Error',
                'SaveLoadService Save Process Error'
            );
        }
        return this;
    }

    public saveClients(clients: ClientsInterface): SaveLoadService
    {

        return this;
    }

    /**
     * TokenをLocalStrageかCookieから読み込む
     * どちらかに存在すれば返す
     * 返却の優先は LocalStrage > Cookie
     * どちらにも存在しない場合はinitialStateTokenを返す
     * 
     * @return Promise<TokenInterface>
     */
    public async loadToken(): Promise<TokenInterface>
    {
        const _ls = await this.loadLocalStrage('token');
        const _ck = await this.loadCookie('token');

        if (this.Token.checkToken(_ls)) return _ls;
        if (this.Token.checkToken(_ck)) return _ck;

        this.SS.setError('Token Load Error');
        return initialStateToken;
    }

    /**
     * TokenをLocalStrageかCookieから読み込む
     * どちらかに存在すれば返す
     * 返却の優先は LocalStrage > Cookie
     * どちらにも存在しない場合はinitialStateTokenを返す
     * 
     * @return Promise<TokenInterface>
     */
    public async loadPersonalToken(): Promise<TokenInterface>
    {
        const _ls = await this.loadLocalStrage('token');
        const _ck = await this.loadCookie('token');

        if (this.Token.checkToken(_ls)) return _ls;
        if (this.Token.checkToken(_ck)) return _ck;

        this.SS.setError('Token Load Error');
        return initialStateToken;
    }

    public async loadClient()
    {

    }

    public checkClient(): boolean
    {
        return true;
    }

    /**
     * Cookieにトークンを保存
     * @param index string
     * @return Promise<boolean>
     */
    private async saveCoockie(index: string): Promise<boolean> {
        return await this.CS.addCookie(index, this.Token.tokenToJson());
    }

    /**
     * LocalStrageにトークンを保存
     * @param index string
     * @return Promise<boolean>
     */
    private async saveLocalStrage(index: string): Promise<boolean> {
        return await this.LS.save(index, this.Token.tokenToJson());
    }

    /**
     * LocalStrageからトークンを読み込み
     * 成功時はJsonデコードしたデータを返す
     * 失敗時はinitialStateTokenを返す
     * 
     * @param index トークンIndexKey
     * @return TokenInterface
     */
    private async loadCookie(
        index: string
    ): Promise<TokenInterface> {
        const _token = await this.CS.getCookieByKey(index);
        if (this.CS.getStatusObj().checkError()) {
            this.SS.setError('Data Load Error to Cookie');
            return initialStateToken;
        } else {
            return this.Token.jsonToToken(_token);
        }
    }

    /**
     * Cookieからトークンを読み込み
     * 成功時はJsonデコードしたデータを返す
     * 失敗時はinitialStateTokenを返す
     * 
     * @param index string トークンIndexKey
     * @return TokenInterface
     */
    private async loadLocalStrage(index: string): Promise<TokenInterface>
    {
        const _token = await this.LS.load(index);
        if (this.LS.getStatusObj().checkError()) {
            this.SS.setError('Data Load Error to LocalStrage');
            return initialStateToken;
        } else {
            return this.Token.jsonToToken(_token);
        }
    }

}