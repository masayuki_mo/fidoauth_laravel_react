import { StringKeyObject, CookieInterface } from './cookie.interface';
import StatusService from '../status/status.service';
export class CookieService implements CookieInterface
{

    private cookie: string = '';
    private cookie_array: StringKeyObject = {};
    private SS: StatusService = new StatusService();

    /**
     * 成否ステータス管理オブジェクトを返す
     * 
     * @return StatusService
     */
    public getStatusObj(): StatusService
    {
        return this.SS;
    }

    /**
     * Cookie情報を上書きする、原則既存のCookieは消える
     * 
     * @param data object
     * @return boolean
     */
    public async setCookie(data: object): Promise<boolean>
    {
        try {
            const st = this.convArrayToText(data);
            document.cookie = st;
            return true;

        } catch (error) {
            this.SS.setError(
                error,
                'Save Error'
            );
            return false;
        }
    }

    /**
     * Cookieにデータを1件追記する
     * 追記データはKeyValue形式
     * 
     * @param key string
     * @param data string
     * @return boolean
     */
    public async addCookie(key: string, data: string): Promise<boolean>
    {
        try {
            this.deleteCookieToKey(key);
            const recode = document.cookie = key + '=' + data;
            document.cookie = recode;
            return true;
        } catch (error) {
            this.SS.setError(
                error,
                'Add Error'
            );
            return false;
        }
    }

    /**
     * 既存のCookieデータの内容を更新
     * ※存在しないデータは追加される
     * 
     * @param data StringKeyObject
     * @return Promise<boolean>
     */
    public async updateCookie(data: StringKeyObject): Promise<boolean>
    {
        try {
            // 全てのCookie情報を取得
            const ar: StringKeyObject = this.getCookieToArray();
            
            // propatieをエスケープしながら文字列に変換
            for (const key in data) {
                if (Object.prototype.hasOwnProperty.call(data, key)) {
                    ar[key] = data[key];
                }
            }
            // 更新
            // console.log(ar);
            document.cookie = await this.convArrayToText(ar);

            return true;
        } catch (error) {
            this.SS.setError(
                error,
                'Update Error'
            );
            return false;
        }
    }

    /**
     * 全てのCookie取得
     * 取得した文字列の状態で返す
     * 
     * @return string 全てのcookie文字列を返す
     */
    public async getCookie(): Promise<string>
    {
        try {
            if (this.cookie === '') {
                this.cookie = await document.cookie;
            }
            return this.cookie;
        } catch (error) {
            this.SS.setError(
                error,
                'Get Error',
            );
            return this.cookie;
        }

    }

    /**
     * Cookieから取り出した値を連想配列に変換したものを返す
     * 
     * @return StingKyeObject
     */
    public async getCookieToArray(): Promise<StringKeyObject>
    {
        // Cookieが既に取得済み、Arrayに分解済みの場合保存データを返す
        if (this.cookie === '') {
            await this.getCookie();
        } else {
            if (this.cookie_array !== {}) {
                return this.cookie_array;
            }
        }

        const _ck = this.cookie.split('; ');

        for (const key in _ck) {
            if (Object.prototype.hasOwnProperty.call(_ck, key)) {
                const _ver = _ck[key].split('=');
                this.cookie_array[_ver[0]] = _ver[1];
            }
        }
        return this.cookie_array;
    }

    /**
     * Keyに一致する値がCookieに含まれている場合
     * レコードの値部分のみ返す
     * 
     * @param key 
     * @return string
     */
    public async getCookieByKey(key: string): Promise<string>
    {
        const c = await this.getCookieToArray();

        for (const k in c) {
            if ( k == key) {
                return c[k];
            }
        }
        return '';
    }

    /**
     * Keyを指定してCookieを削除
     * 
     * @param key 
     */
    public async deleteCookieToKey(key: string): Promise<boolean>
    {
        // propatieをエスケープしながら文字列に変換
        if (key in this.cookie_array) {
            document.cookie = key + '=; max-age=0';
            document.cookie = key + '=; expires=' + this.getPastExpiresTime();
            await this.refreshCookie();
        }
        return true;
    }

    /**
     * 全てのCookieを削除
     * 有効期限を過去に指定して削除する
     */
    public async deleteCookieAll()
    {
        let _var_age = '';
        let _var_expires = '';

        for(var key in this.cookie_array){
            if (Object.prototype.hasOwnProperty.call(this.cookie_array, key)) {
                _var_age = _var_age + key + '=;max-age=0';
                _var_expires = _var_expires + key + '=;expires=' + this.getPastExpiresTime();
            }
        }

        try {
            document.cookie = _var_age;
            document.cookie = _var_expires;

            return true;
        } catch (error) {
            console.error('Token Delete Error: ' + error);
            this.SS.setStatus(
                error,
                'Token Delete Error'
            );
            return false;
        }
    }

    /**
     * Cookie情報を更新する
     * 
     * @reuturn CookieService
     */
    public async refreshCookie(): Promise<boolean>
    {
        this.cookie = '';
        this.cookie_array = {};
        const result = await this.getCookieToArray();
        return true;
    }

    /**
     * 配列、オブジェクトデータを文字列に変換して返す
     * 
     * @param data object
     * @return string
     */
    private convArrayToText(data: StringKeyObject): string
    {

        let st = '';
        // propatieをエスケープしながら文字列に変換
        for (const key in data) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
                if (st !== '') {
                    st = st + ';'
                }
                st = st + key + '=' + encodeURIComponent(data[key]);
            }
        }

        return st;
    }

    /**
     * Cookie削除用に、1年前の古い日付を返す
     * 
     * @return string Dateオブジェクトの日付
     */
    private getPastExpiresTime(): string
    {
        const now: any = new Date();
        now.setYear(now.getYear() - 1);
        return now.toGMTString();
    }
}