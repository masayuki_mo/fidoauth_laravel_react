
import { LocalStrageInterface } from './localstrage.interface';
import StateusService from '../status/status.service';

export class LocalStrageService implements LocalStrageInterface
{
    private SS: StateusService = new StateusService();

    /**
     * 成否ステータス管理オブジェクトを返す
     * 
     * @return StatusService
     */
    public getStatusObj(): StateusService
    {
        return this.SS;
    }

    /**
     * LocalStrageにデータ保存
     * 文字列データをKye Value形式で保存
     * 
     * @param key string キー
     * @param data string 文字列データ
     * @return Promise<boolean>
     */
    public async save(key: string, data: string): Promise<boolean>
    {
        try {
            await localStorage.setItem(key, data);
            return true;
        } catch (error) {
            this.SS.setError(
                error,
                'Save Error'
            );
            return false;
        }
    }

    /**
     * JSONデータを保存
     * 保存時に文字列に変換
     * 
     * @param key string キー
     * @param obj string JSONデータ
     * @return Promise<boolean>
     */
    public async saveToJson(key: string, obj: any): Promise<boolean>
    {
        return await this.save(key, this.toJson(obj));
    }

    /**
     * データ取得（文字列データ）
     * @param key string キー
     * @return Promise<string>
     */
    public async load(key: string): Promise<string>
    {
        try {
            const st = await localStorage.getItem(key);
            return (st) ? st : '';
        } catch (error) {
            this.SS.setError(
                error,
                'Load Error'
            );
            return '';
        }

    }

    /**
     * データ取得（JSONデータ）
     * 保存データがJSON文字列の場合オブジェクトにデコードして返す
     * @param key string キー
     * @return JsonObject
     */
    public async loadToJson(key: string): Promise<object>
    {
        return this.fromJson(await this.load(key));
    }

    /**
     * キーが存在した場合はデータを返す、存在しない場合はFalseを返す
     * @param key 
     * @return string | boolean
     */
    public async searchKey(key: string): Promise<string|boolean>
    {
        for (var i = 0; i < localStorage.length; i++) {
            if (localStorage.key(i) === key) {
                return await this.load(key);
            }
        }
        return false;
    }

    /**
     * データ削除
     * @param key string キー
     * @return LocalStrageService
     */
    public async delete(key: string): Promise<this>
    {
        try {
            await localStorage.removeItem(key);
            return this;
        } catch (error) {
            this.SS.setError(
                error,
                'Delete Error'
            );
            return this;
        }

    }

    /**
     * データを全て削除
     * @return LocalStrageService
     */
    public async deleteAll(): Promise<this>
    {
        try {
            await localStorage.clear();
            return this;
        } catch (error) {
            this.SS.setError(
                error,
                'Clear Error'
            );
            return this;
        }

    }

    /**
     * JSONデータを文字列に変換
     * @param obj JSON
     * @return string
     */
    private toJson(obj: Object): string {
        return JSON.stringify(obj);
    }

    /**
     * 文字列をJSONに変換
     * JSON文字列でなかった場合
     * Key「undefined」のオブジェクトで返す
     * 
     * @param txt string 文字列
     * @return JSON
     */
    private fromJson(txt: string): object {
        try {
            const _txt = JSON.parse(txt);
            return _txt;
        } catch (error) {
            this.SS.setError(
                error,
                'JSON Parce Error'
            );
            return { undefined: txt };
        }
    }
}
