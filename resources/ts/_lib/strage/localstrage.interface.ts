
export interface LocalStrageInterface {
    /**
     * データ保存
     * @param key string キー
     * @param data string データ
     * @return LocalStrageService
     */
    save(key: string, data: any): Promise<boolean>;

    /**
     * JSONデータを保存
     * @param key string キー
     * @param obj string JSON
     * @return LocalStrageService
     */
    saveToJson(key: string, obj: any): Promise<boolean>;

    /**
     * データ取得（文字列データ）
     * @param key string キー
     * @return LocalStrageService
     */
    load(key: string): Promise<string>;

    /**
     * データ取得（JSONデータ）
     * @param key string キー
     * @return Json
     */
    loadToJson(key: string): Promise<object>;

    /**
     * キーが存在した場合はデータを返す、存在しない場合はFalseを返す
     * @param key 
     * @return string | boolean
     */
    searchKey(key: string): Promise<string|boolean>;

    /**
     * データ削除
     * @param key string キー
     * @return LocalStrageService
     */
    delete(key: string): Promise<this>;

    /**
     * データを全て削除
     * @return LocalStrageService
     */
    deleteAll(): Promise<this>;
}