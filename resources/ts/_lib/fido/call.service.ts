import AjaxHelper from '../http/ajax.service';

import {
    FidoInterface, initialFido
} from './fido.interface';

import {
    convertCredentialCreateOptions,
    convertCredentialRequestOptions
} from './credential.service';

import {
    Param, Paths
} from './config.service';

/**
 * Fidoトークン管理
 * 
 */
export class CallService
{
    private aj: AjaxHelper;
    private PARAM: Param;
    private PAYLOAD: FidoInterface;
    private _result: any;

    constructor()
    {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
        this.PAYLOAD = initialFido;
    }

    /**
     * サーバーからの戻りデータを返す
     * 
     * @return FidoInterface
     */
    public getPayload(): FidoInterface
    {
        return this.PAYLOAD;
    }

    /**
     * API呼び出し用オプションを設定
     * 
     * @param data any ServerInterface
     * @return FidoService
     */
    public setServerParam(data: any): CallService
    {
        this.PARAM.resetParam();
        this.PARAM.setParam(data);
        return this;
    }

    /**
     * サーバー返却データをBodyに設定する
     * 
     * @param job string 'login' | 'regist'
     * @return Promise<CallService>
     */
    public async setReaponceData(job: string): Promise<CallService>
    {
        this.aj
            .resetRequest()
            .setBody(
                await this.buildClient(job)
            );
        return this;
    }

    /**
     * サーバー応答の判定
     * 
     * @return boolean
     */
    public checkResult(): boolean {
        if (this._result === 'success') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * サーバーAPIを呼び出す
     * 
     * @param job string ジョブ名[regist,login,check,update,delete]
     * @return Promise<void> Promiseイベントだけ返す
     */
    public async callAPI(job: string): Promise<void> {
        const p:{[key:string]: any} = this.PARAM.buildParam();
        const op = Paths[job];

        const result = await this.aj
                    .setURL(p.url + op.path)
                    .setMethod(op.method)
                    .setHeader(p.header)
                    .buildRequestParam()
                    .getResult();
        this._result = result.type;
        this.PAYLOAD = result.message;
    }

    /**
     * クライアントクレデンシャルを生成
     * 
     * @return Promise<object> クライアントクレデンシャル
     */
    private async buildClient(job: string): Promise<object>
    {
        const options = await this.getCredentialOptions(job); 
        let credential: any;
        let _id = {};
        if (job === 'regist') {
            credential = await convertCredentialCreateOptions({
                publicKey: options
            });
            _id = {
                registrationId  : this.PAYLOAD.registrationId,
                current         : this.PARAM.getCurrent(),
            };
        }
        if (job === 'login') {
            credential = await convertCredentialRequestOptions({
                publicKey: options
            });
            _id = {
                assertionId : this.PAYLOAD.assertionId,
                current         : this.PARAM.getCurrent(),
            };
        }

        return {
            credential,
            ..._id
        };
    }

    /**
     * 戻りデータからCredentialOptionsを返す
     * 
     * @return any
     */
    private async getCredentialOptions(job: string): Promise<any>
    {
        this.PARAM.setFido(this.PAYLOAD)
        if (job === 'regist') return this.PARAM.getCredentialCreationOptions()
        if (job === 'login') return  this.PARAM.getCredentialRequestOptions()
    }
}