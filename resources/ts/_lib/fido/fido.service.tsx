import { CallService } from './call.service';

/**
 * Fidoトークン管理
 * 
 */
export default class FidoService
{
    private CS: CallService;

    constructor()
    {
        this.CS = new CallService();
    }

    /**
     * API呼び出し用オプションを設定
     * 
     * @param data any ServerInterface
     * @return FidoService
     */
    public setServerParam(data: any): FidoService
    {
        this.CS.setServerParam(data)
        return this;
    }

    /**
     * 保存中のFido情報を返す
     * 
     * @return any
     */
    public getFido() {
        return this.CS.getPayload();
    }

    /**
     * Fido登録開始、クライアントクレデンシャルを作成
     * クレデンシャルの生成結果をBooleanで返す
     * 
     * @return Promise<boolean> 処理の成否
     */
    public async startFidoRegistProcess(): Promise<boolean>
    {
        await this.CS.callAPI('registStart');
        if (this.CS.checkResult()) {
            await this.CS.setReaponceData('regist');
            return true;
        }
        return false;
    }

    /**
     * クライアントクレデンシャルをサーバーに返す
     * サーバー応答をBooleanで返す
     * 
     * @return Promise<boolean> サーバー応答成否
     */
    public async responceFidoRegistProcess(): Promise<boolean>
    {
        await this.CS.callAPI('registResponce');
        return this.CS.checkResult();
    }

    /**
     * Fidoログイン開始、リクエストクレデンシャルを作成
     * 作成の成否をBooleanで返す
     * 
     * @return Promise<boolean>
     */
    public async startFidoLoginProcess(): Promise<boolean>
    {
        // サーバーにログインの要求を投げる
        await this.CS.callAPI('loginStart');

        // 成功時にクレデンシャルを作成し返却用オプションに設定する
        if (this.CS.checkResult()) {
            await this.CS.setReaponceData('login');
            return true;
        }
        return false;
    }

    /**
     * リクエストクレデンシャルをサーバーに返す
     * サーバー応答をBooleanで返す
     * 
     * @return Promise<boolean>
     */
    public async responceFidoLoginProcess(): Promise<boolean>
    {
        await this.CS.callAPI('loginResponce');
        return this.CS.checkResult();
    }

    /**
     * FidoKeyが登録済みか確認
     * 
     * @return Promise<boolean>
     */
    public async checkRegist(): Promise<boolean>
    {
        await this.CS.callAPI('check');
        return this.CS.checkResult();
    }

    /**
     * 認証有効期限の確認
     * 
     * @return Promise<boolean>
     */
    public async checkExpired(): Promise<boolean>
    {
        await this.CS.callAPI('expired');
        return this.CS.checkResult();
    }

    /**
     * FidoKeyの登録削除
     * 
     * @return Promise<boolean>
     */
    public async deleteCredential(): Promise<boolean>
    {
        await this.CS.callAPI('delete');
        return this.CS.checkResult();
    }

    /**
     * ログアウト処理開始
     * 
     * @return Promise<boolean>
     */
    public async logoutStart(): Promise<boolean>
    {
        await this.CS.callAPI('logout');
        return this.CS.checkResult();
    }
}
