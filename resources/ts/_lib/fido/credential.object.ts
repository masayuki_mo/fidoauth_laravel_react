export const copy = 'copy';
export const convert = 'convert';
type SchemaLeaf = "copy" | "convert";
interface SchemaObject { [property: string]: { required: boolean; schema: Schema; }; }
type SchemaArray = [SchemaObject] | [SchemaLeaf];
export type Schema = SchemaLeaf | SchemaArray | SchemaObject;

export const publicKeyCredentialDescriptorSchema: Schema = {
    type: required(copy),
    id: required(convert),
    transports: optional(copy),
};

export function required(schema: Schema): any {
    return {
      required: true,
      schema,
    };
}
  
export function optional(schema: Schema): any {
    return {
      required: false,
      schema,
    };
}


export const simplifiedExtensionsSchema: Schema = {
    appid: optional(copy),
};

/**
 * navigator.createに渡すオブジェクト
 * サーバー受取時にbase64urlから
 * ByteArrayに変換するプロパティを「required(convert)」指定する
 */
export const credentialCreationOptions: Schema = {
    publicKey: required({
      rp: required(copy),
      user: required({
        id: required(convert),
        name: required(copy),
        displayName: required(copy),
        icon: optional(copy),
      }),
  
      challenge: required(convert),
      pubKeyCredParams: required(copy),
  
      timeout: optional(copy),
      excludeCredentials: optional([publicKeyCredentialDescriptorSchema]),
      authenticatorSelection: optional(copy),
      attestation: optional(copy),
      extensions: optional(simplifiedExtensionsSchema),
    }),
    signal: optional(copy),
};
  
/**
 * navigator.createから返却されるオブジェクト
 * サーバー送信時ににByteArrayから
 * base64urlに変換するプロパティを「required(convert)」指定する
 */
  
export const publicKeyCredentialWithAttestation: Schema = {
    type: required(copy),
    id: required(copy),
    rawId: required(convert),
    response: required({
      clientDataJSON: required(convert),
      attestationObject: required(convert),
    }),
};
  
/**
 * navigator.getに渡すオブジェクト
 * サーバー受取時にbase64urlから
 * ByteArrayに変換するプロパティを「required(convert)」指定する
 */
export const credentialRequestOptions: Schema = {
    mediation: optional(copy),
    publicKey: required({
      challenge: required(convert),
      timeout: optional(copy),
      rpId: optional(copy),
      allowCredentials: optional([publicKeyCredentialDescriptorSchema]),
      userVerification: optional(copy),
      extensions: optional(simplifiedExtensionsSchema),
    }),
    signal: optional(copy),
};
  
/**
 * navigator.getから返却されるオブジェクト
 * サーバー送信時にByteArrayから
 * base64urlに変換するプロパティを「required(convert)」指定する
 */
export const publicKeyCredentialWithAssertion: Schema = {
    type: required(copy),
    id: required(copy),
    rawId: required(convert),
    response: required({
      clientDataJSON: required(convert),
      authenticatorData: required(convert),
      signature: required(convert),
      userHandle: required(convert),
    }),
};