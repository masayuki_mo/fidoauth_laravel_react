import {
    FidoInterface, initialFido,
    ServerInterface, initialStateServer
} from './fido.interface';

export const Paths: {[key:string]: any} = 
{
    // 登録作成開始
    registStart     : {
        path: 'api/fido/regist_start',
        method: 'POST'
    },
    // 登録データ送信
    registResponce  : {
        path: 'api/fido/regist_finish',
        method: 'POST'
    },
    // ログインデータ作成開始
    loginStart      : {
        path: 'api/fido/assertion_start',
        method: 'POST'
    },
    // ログインデータ送信
    loginResponce   : {
        path: 'api/fido/assertion_finish',
        method: 'POST'
    },
    // ログアウト
    logout   : {
        path: 'api/fido/logout',
        method: 'POST'
    },
    // 登録済み確認
    check           : {
        path: 'api/fido/check',
        method: 'POST'
    },
    // 有効期限切れ確認
    expire          : {
        path: 'api/fido/expired',
        method: 'POST'
    },
    // 登録更新
    update          : {
        path: 'api/fido/update',
        method: 'UPDATE'
    },
    // 登録削除
    delete          : {
        path: 'api/fido/delete',
        method: 'POST'
    },
};

export class Param
{
    // APIコールオプション
    private PARAM: ServerInterface = initialStateServer;
    private FIDO: FidoInterface = initialFido;

    private _ORG: any = {};

    /**
     * APIコールオプションを全て返す
     * 
     * @return ServerInterface
     */
    public getParam(): ServerInterface
    {
        return this.PARAM;
    }

    /**
     * FIDOコールオプションを全て返す
     * 
     * @return FidoInterface
     */
    public getFido(): FidoInterface
    {
        return this.FIDO;
    }

    /**
     * 認証後の戻りURLを返す
     * 
     * @return string 認証後の戻りURL文字列
     */
    public getCurrent(): string
    {
        return (this.PARAM.current) ? this.PARAM.current : '';
    }

    /**
     * CredentialCreationOptionsを返す
     * 
     * @return any CredentialCreationOptions
     */
    public getCredentialCreationOptions(): any
    {
        return this.FIDO.publicKeyCredentialCreationOptions;
    }

    /**
     * CredentialRequestOptionsを返す
     * 
     * @return any CredentialRequestOptions
     */
    public getCredentialRequestOptions(): any
    {
        return this.FIDO.publicKeyCredentialRequestOptions;
    }
    
    /**
     * パラメータをセットする
     * 雛形パラメータに合致するキーのみ更新
     * 
     * @param data サーバーオプションと思われるデータ
     * @return Param
     */
    public setParam(data: any): Param
    {
        for (const key in this.PARAM) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
                this.PARAM[key] = data[key];
            }
        }

        return this;
    }

    /**
     * APIアクセス用トークンを設定
     * 
     * @param bearer Bearerトークン
     * @param csrf CSRFトークン
     * @return Param
     */
    public setToken(bearer: string, csrf: string): Param
    {
        this.PARAM.bearer = bearer;
        this.PARAM.csrf = csrf;
        return this;
    }

    /**
     * FIDOオプションを設定する
     * initialStateと一致するキーのみ更新
     * 
     * @param data any FIDOオプションと思われるデータ
     * @return Param
     */
    public setFido(data: any): Param
    {
        for (const key in this.FIDO) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
                this.FIDO[key] = data[key];
            }
        }
        return this;
    }

    /**
     * APIコールオプションを作成
     * @return ServerInterface
     */
    public buildParam(): object {
        const header = [];
        // const p = this.PARAM.getParam();
        header.push(
            'X-CSRF-TOKEN: ' + this.PARAM.csrf,
            'Authorization: Bearer ' + this.PARAM.bearer
        );

        return {
            url     : this.PARAM.url,
            method  : this.PARAM.method,
            header  : header.concat(this.PARAM.header),
            body    : this.PARAM.body,
            bearer  : this.PARAM.bearer,
            csrf    : this.PARAM.csrf,
        }
    }

    /**
     * API呼び出し用オプションを初期化
     * 
     * @return Param
     */
    public resetParam(): Param
    {
        this.PARAM = initialStateServer;
        return this;
    }

    /**
     * Fido情報を初期化
     * 
     * @return Param
     */
    public resetFido(): Param
    {
        this.FIDO = initialFido;
        return this;
    }

    /**
     * API呼び出しオプション、Fido情報を初期化
     * 
     * @return Param
     */
    public resetAll(): Param
    {
        this.resetParam();
        this.resetFido();
        return this;
    }

}



