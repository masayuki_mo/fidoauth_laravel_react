
export interface FidoInterface
{
    Fido?                               : any;
    publicKeyCredentialCreationOptions? : any;
    registrationId?                     : any;
    publicKeyCredentialRequestOptions?  : any;
    assertionId?                        : string;
    credentialResponse?                 : any;
    requestPath?                        : any;
    fidoJob?                            : string;
    dispatch?                           : any;
    [key: string]                       : any;
}


export interface TokenInterface
{
    access_token    : string,
    refresh_token   : string,
    expires_in      : number,
    token_type      : string,
    [key: string]   : string | number,
};

export interface ServerInterface
{
    bearer? : string,
    csrf?   : string,
    current?: string,
    url     : string,
    method  : string,
    header  : Array<string>,
    body    : any,
    [key: string]: any,
}

export const initialFido = {
    Fido                               : {},
    publicKeyCredentialCreationOptions : {},
    registrationId                     : {},
    publicKeyCredentialRequestOptions  : {},
    assertionId                        : '',
    credentialResponse                 : {},
    requestPath                        : {},
    fidoJob                            : '',
    dispatch                           : {},
}

export const initialStateToken = {
    access_token    : '',
    refresh_token   : '',
    expires_in      : 1800,
    token_type      : '',
};


export const initialStateServer = {
    bearer  : '',
    csrf    : '',
    current : '',
    url     : 'https://yasukosan.dip.jp/laravel-2fa-fido/public/',
    method  : 'POST',
    header  : [
                'Content-type: application/json',
                'Accept: application/json'
            ],
    body    : '',
}
