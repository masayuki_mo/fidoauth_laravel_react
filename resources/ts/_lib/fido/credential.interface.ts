import {
    Base64urlString
} from './converter.service';
/**
 * 認証時にクライアントからサーバーへ送るリクエストの雛形
 */
export interface AuthCredentialRequest {
    username: string;
    userVerification: string;
}



/**
 * navigator.create navigator.get
 * 両方で使用する雛形
 */
export interface PublicKeyCredentialDescriptorJSON {
    type: PublicKeyCredentialType;
    id: Base64urlString;
    transports?: AuthenticatorTransport[];
}
export interface SimpleWebAuthnExtensionsJSON {
    appid?: string;
}


/**
 * ユーザー作成（navigator.create）
 * サーバーリクエストで使用する雛形
 * 
 * 以下3つがネストされている
 * CredentailCreationOptionsJSON
 * - PublicKeyCredentialCreationOptionsJSON
 *  - PublicKeyCredentialUserEntityJSON
 */

// navigator.createに渡すオブジェクトの雛形
export interface CredentialCreationOptionsJSON {
    publicKey: PublicKeyCredentialCreationOptionsJSON;
    signal?: AbortSignal;
}
// CredentialCreationOptionsJSONのpublicKeyオブジェクトの雛形
export interface PublicKeyCredentialCreationOptionsJSON {
    rp: PublicKeyCredentialRpEntity;
    user: PublicKeyCredentialUserEntityJSON;
  
    challenge: Base64urlString;
    pubKeyCredParams: PublicKeyCredentialParameters[];
  
    timeout?: number;
    excludeCredentials?: PublicKeyCredentialDescriptorJSON[];
    authenticatorSelection?: AuthenticatorSelectionCriteria;
    attestation?: AttestationConveyancePreference;
    extensions?: SimpleWebAuthnExtensionsJSON;
}

// PublicKeyCredentialCreationOptionsのuserオブジェクトの雛形
export interface PublicKeyCredentialUserEntityJSON extends PublicKeyCredentialEntity {
    displayName: string;
    id: Base64urlString;
}

/**
 * ユーザー作成（navigator.create）
 * クライアントレスポンスで使用する雛形
 * 
 * 以下２つがネストされる
 * AuthenticatorAttestationResponseJSON
 * - PublicKeyCredentialWithAttestationJSON
 */

export interface AuthenticatorAttestationResponseJSON {
    clientDataJSON: Base64urlString;
    attestationObject: Base64urlString;
}

export interface PublicKeyCredentialWithAttestationJSON {
    id: string;
    type: PublicKeyCredentialType;
    rawId: Base64urlString;
    response: AuthenticatorAttestationResponseJSON;
  }


/**
 * ユーザー認証（navigator.get）
 * サーバーリクエストで使用される雛形
 * 
 * 以下２つがネストされる
 * CredentialRequestOptionsJSON
 * - PublicKeyCredentialDescriptorJSON
 */
export interface CredentialRequestOptionsJSON {
    mediation?: CredentialMediationRequirement;
    publicKey?: PublicKeyCredentialRequestOptionsJSON;
    signal?: AbortSignal;
}
export interface PublicKeyCredentialRequestOptionsJSON {
    challenge: Base64urlString;
    timeout?: number;
    rpId?: string;
    allowCredentials?: PublicKeyCredentialDescriptorJSON[];
    userVerification?: UserVerificationRequirement;
    extensions?: SimpleWebAuthnExtensionsJSON;
}


/**
 * ユーザー認証（navigator.get）
 * クライアントレスポンスで使用される雛形
 * 
 * 以下２つがネストされる
 * AuthenticatorAssertionResponseJSON
 * - PublicKeyCredentialWithAssertionJSON
 */

export interface AuthenticatorAssertionResponseJSON {
    clientDataJSON: Base64urlString;
    authenticatorData: Base64urlString;
    signature: Base64urlString;
    userHandle: Base64urlString | null;
}

export interface PublicKeyCredentialWithAssertionJSON {
    type: PublicKeyCredentialType;
    id: string;
    rawId: Base64urlString;
    response: AuthenticatorAssertionResponseJSON;
}