
# ファイル参照構造
```
ajax.service
    |
    |- ajax.parse.service
        |
        |- responce.service
        |
        |- responce.header.service
        |
        |- responce.body.service
    |
    |- request.service
```

# サンプル


## URLにGET要求を投げる

```javascript

const as = new AjaxService;
// getResultメソッドはサーバーレスポンスからbodyのみを返す
const result = as.setURL('http://example.com')
                .setMethod('GET')
                .getResult();

console.log(result);

```

## レンスポンスコードを確認し処理を変える

```javascript
const as = new AjaxService;
const result = as.setURL('http://example.com')
                .setMethod('GET')
                .getResult();

const response = as.callAjaxParseService().getResponse();
// 成功
if (response.status === 200) {
    console.log(result);
}
// エラー
if (response.status === 500) {
    console.error(result);
}

```

