## Fido2認証

# プロジェクトクローン後、下記を実行  
docker-compose up -d
docker-compose exec web /var/tmp/setup.sh


# ローカル実行の場合
composer install

※ memory limitエラーが出る場合は下記を別途実行
 php -d memory_limit=-1 C:/ProgramData/ComposerSetup/bin/composer.phar require laravel/passport:9.*





# 初回セットアップ情報
なにかの間違いで0から環境を整備する必要が出た場合

・Laravelインストール
composer create-project "laravel/laravel=7.*" .
・パスポートインストール
php -d memory_limit=-1 C:/ProgramData/ComposerSetup/bin/composer.phar require laravel/passport:9.*
・reactフロントエンド有効化
php .\artisan ui react --auth
・npmモジュールインストール
npm install
npm run dev

