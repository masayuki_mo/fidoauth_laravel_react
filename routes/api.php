<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/register', '_mo\AuthController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function() {
    Route::post('fido/regist_start', '_mo\RegisterController@register');
    Route::post('fido/regist_finish', '_mo\RegisterController@registrationFinish');
    
    Route::post('fido/assertion_start', '_mo\AuthController@authenticate');
    Route::post('fido/assertion_finish', '_mo\AuthController@authenticateFinish');

    Route::post('fido/logout',  '_mo\FidoController@logout');
    Route::post('fido/check',   '_mo\FidoController@checkRegist');
    Route::post('fido/expired', '_mo\FidoController@checkExpired');
    Route::post('fido/update',  '_mo\FidoController@checkCirtificate');
    Route::post('fido/delete',  '_mo\FidoController@deleteCredential');
});



 