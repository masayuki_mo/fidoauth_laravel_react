<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\_mo\Model\AppUser;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Fidoユーザー追加時に生成したHashキーを登録する
     *
     * @param integer $id
     * @param array $hash
     * @return void
     */
    public static function addUserHash(int $id, array $hash)
    {
        $_user = self::where('id', $id)->first();
        $_user->app_user_id = $hash['userId'];
        $_user->save();
    }

    /**
     * Fidoユーザーのハッシュキーを消す
     *
     * @param integer $id
     * @return void
     */
    public static function deleteUserHash(int $id)
    {
        $_user = self::where('id', $id)->first();
        $_user->app_user_id = '';
        $_user->save();
    }


    public static function getUserById(int $id)
    {
        return self::where('id', $id)->first();
    }

    /**
     * AppUserモデルとリレーション
     *
     * @return AppUser
     */
    public function appUser()
    {
        return $this->hasOne('App\_mo\Model\AppUser', 'userid', 'app_user_id');
    }
    /**
     * Crdentialモデルとリレーション
     *
     * @return AppUser
     */
    public function credentials()
    {
        return $this->hasOne('App\_mo\Model\Credentials', 'id', 'userid');
    }
}
