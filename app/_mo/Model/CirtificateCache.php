<?php

namespace App\_mo\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * cirtificate_cache
 * 
 * app_user_id              bigint(20)  NOTNULL NONE
 * expires_out              timestamp   NOTNULL current_timestamp
 */ 

class CirtificateCache extends Model
{
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'cirtificate_cache';

    /**
     * キャッシュデータ登録
     * 既にキャッシュが存在する場合は削除後に登録
     * ※既存キャッシュがある場合は実質延長
     *
     * @param array $cache 登録データ
     * @return CirtificateCache
     */
    public static function addCache(array $cache)
    {
        // 先にチェックを掛けて、有効なデータが存在する場合消す
        if (self::checkCache($cache['app_user_id'])) {
            self::deleteCache($cache['app_user_id']);
        }

        $_cache = new self;
        $_cache->app_user_id    = $cache['app_user_id'];
        $_cache->expires_out    = new Carbon('+2 minutes');
        $_cache->save();
    }

    /**
     * キャッシュの削除
     *
     * @param integer $id app_user_id
     * @return void
     */
    public static function deleteCache(string $id)
    {
        self::where('app_user_id', $id)->delete();
    }

    /**
     * 有効なキャッシュデータが存在するか
     *
     * @param integer $id app_user_id
     * @return bool
     */
    public static function checkCache(string $id): bool
    {
        try {
            $cache = self::where('app_user_id', $id)->first();
            if (self::checkExpires($cache->expires_out)) {
                return true;
            } else {
                self::deleteCache($id);
                return false;
            }
        } catch (\Throwable $th) {
            return false;
        }

        return false;
    }

    /**
     * 有効期限切れのキャッシュを全て削除
     * ※ 30分以上経過したキャッシュを削除
     *
     * @return void
     */
    public static function creanCache()
    {
        self::whereDate(
            'expires_out', '<=', Carbon::now('Asia/Tokyo')
            )->delete();
    }

    /**
     * 有効期限が過ぎているか判定
     *
     * @param DateTime $time 有効期限
     * @return boolean 
     */
    private static function checkExpires(string $time): bool
    {
        $_expire = new Carbon($time);
        $_now = new Carbon();
        if ($_now->gt($_expire)) {
            return false;
        } else {
            return true;
        }
    }
}
