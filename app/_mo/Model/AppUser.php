<?php

namespace App\_mo\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\_mo\Model\Credentials;

/**
 * app_user
 * 
 * id                       bigint(20)      NOTNULL NONE    AUTO_INCREMENT
 * username                 varchar(255)    NOTNULL NONE    ユーザー名
 * userid                   varchar(255)    NOTNULL NONE    ユーザーID
 * registration_start       timestamp       NOTNULL current_timestamp() 登録開始時間
 * registration_token       varchar(255)    NOTNULL None    登録用トークン
 */
class AppUser extends Model
{
    protected $table = 'app_user';
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        'username',
        'userid',
        'registration_start',
        'registration_token'
    ];


    /**
     * Undocumented function
     *
     * @param array $appUser
     * @return self
     */
    public static function addAppUser(array $appUser): void
    {
        $_appUser = new self;
        $_appUser->username                 = $appUser['username'];
        $_appUser->userid                   = $appUser['userid'];
        $_appUser->registration_start       = Carbon::now();
        $_appUser->registration_token       = $appUser['registration_token'];
        $_appUser->save();
    }

    /**
     * IDからレコードを更新する
     *
     * @param array $appUser
     * @return void
     */
    public static function updateAppUserById(array $appUser): void
    {
        $_appUser = self::where('id', $appUser['id']);
        $_appUser->username                 = $appUser['username'];
        $_appUser->userid                   = $appUser['userid'];
        $_appUser->registration_start       = Carbon::now();
        $_appUser->registration_token       = $appUser['registration_token'];
        $_appUser->save();
    }

    /**
     * 認証した時間を更新
     *
     * @param array $appUser
     * @return variant_mod
     */
    public static function updateRegistrationStartById(array $appUser): variant_mod
    {
        $_appUser = self::where('id', $appUser['id']);
        $_appUser->registration_start = Carbon::now();
        $_appUser->save();
    }

    /**
     * IDからレコードを削除する
     *
     * @param integer $id
     * @return void
     */
    public static function deleteAppUserById(int $id): void
    {
        self::where('id', $id)->delete();
    }

    /**
     * ユーザー名からレコードを削除する
     *
     * @param string $username
     * @return void
     */
    public static function deleteAppUserByUserId(string $userid): void
    {
        self::where('userid', $userid)->delete();
    }

    /**
     * AppUserテーブルの情報を全て削除する
     *
     * @return void
     */
    public static function deleteAppUserAll(): void
    {
        self::query()->delete();
    }

    /**
     * registrationTokenからユーザー情報を1件取得し返す
     *
     * @param string $registrationToken
     * @return object
     */
    public static function getAppUserByRegistrationToken(
        string $registrationToken
    ): object {
        return self::where('registration_token', $registrationToken)
                ->first();
    }

    /**
     * UserIdからユーザー情報を1件取得し返す
     *
     * @param string $userid
     * @return object
     */
    public static function getAppUserByUserId(
        string $userid
    ): object {
        return self::where('userid', $userid)
                ->first();
    }

    /**
     * UserNameからユーザー情報を1件取得し返す
     *
     * @param string $username
     * @return object | null
     */
    public static function getAppUserByUserName(string $username)
    {
        return self::where('username', $username)->first();
    }

    /**
     * ユーザー名からAppUserにCredential情報を結合したデータを1件返す
     *
     * @param string $username
     * @return object
     */
    public static function getAppUserWithCredentialsByUsername(string $username): object
    {
        return self::where('username', $username)
                ->rightJoin('credentials', 'app_user.id', '=', 'credentials.app_user_id')
                ->get()
                ->first();
    }

    /**
     * ユーザーIDからAppUserにCredentail情報を結合したデータを1件返す
     *
     * @param string $userid
     * @return object
     */
    public static function getAppUserWithCredentialsByUserId(string $userid): object
    {
        return self::where('userid', $userid)
                ->rightJoin('credentials', 'app_user.id', '=', 'credentials.app_user_id')
                ->get()
                ->first();
    }

    /**
     * 登録用AppUserデータを作成し返す
     *
     * @param string $username
     * @param array $credentialHash
     * @return array
     */
    public static function buildRegistData(
        string $username, array $credentialHash): array
    {
        return array(
            'username'              => $username,
            'userid'                => $credentialHash['userId'],
            'registration_start'    => Carbon::now(),
            'registration_token'    => $credentialHash['registrationId']
        );
    }

    /**
     * ユーザーが登録済みか判定し返す
     *
     * @param string $username
     * @return bool
     */
    public static function checkNewUser(string $username): bool
    {
        $user = AppUser::where('username', $username)->get();
        if (!$user->isEmpty()) {
            return false;
        }
        return true;
    }
    

    /**
     * Credentailsとのリレーション用
     *
     * @return object
     */
    public function credential()
    {
        // return $this->hasOne('App\Credentials', 'id', 'app_user_id');
        return $this->hasOne('App\Credentials', 'app_user_id');
    }
}
