<?php

namespace App\_mo\Model;

use Illuminate\Support\Facades\Auth;
use App\_mo\Model\CirtificateCache;
use App\_mo\Model\AppUser;
use App\_mo\Model\Credentials;
use App\User;
use App\Config;
use Carbon\Carbon;

/**
 * Fido 
 * 
 */ 

class FidoU2F
{
    /**
     * FidoKeyが登録済みか確認
     *
     * @return boolean 
     */
    public static function checkRegistered(): bool
    {
        $user = Auth::user();
        if ($user->appUser) {
            return true;
        }

        return false;
    }

    /**
     * ログインから指定時間経過しているか判定
     *
     * @param Request $request
     * @return string True: certified False: uncirtified
     */
    public static function checkExpired()
    {
        $user = Auth::user();
        // return $user->appUser->userid;
        if (CirtificateCache::checkCache($user->appUser->userid))
        {
            return true;
        }
        return  false;
    }

    /**
     * 障害、エラー、例外により登録途中で残ったゴミデータを消す
     * ・何も登録が無い（検出できないも含む）
     * ・全てのテーブルに登録データが有る（通常通り登録出来ている）
     * 場合は削除は行わない
     *
     * @return void
     */
    public static function deleteMissData()
    {
        // 残骸の数をカウント
        $checkCounter = 0;
        $uc = false;    // Userテーブルの残骸フラグ
        $au = false;    // AppUser残骸フラグ
        $cr = false;    // credentials残骸フラグ
    
        // Userの残骸確認
        $user = Auth::user();
        self::checkUserWreckage($user, $uc, $checkCounter);
        // AppUserの残骸確認
        $appuser = self::checkAppUserWreckage(
                        $user->name, $au, $checkCounter
                    );
        
        // appuserデータが存在する場合のみ残骸確認を続ける
        if ($appuser !== false) {
            // Credentialsの残骸確認
            $credential = self::checkCredentialsWreckage(
                                $appuser->userid, $cr, $checkCounter
                            );
        }

        /**
         * カウンター毎の処理
         * ０：何もしない   (何も処理していない)
         * １～２：削除実行 (中途半端に終っている)
         * ３：何もしない   (すべての処理が完了している)
         */ 
        if ($checkCounter === 1 || $checkCounter === 2) {
            if ($cr) Credentials::deleteCredentialById($credential->id);
            if ($au) AppUser::deleteAppUserByUserId($user->app_user_id);
            if ($uc) User::deleteUserHash($user->id);
        }
        return User::getUserById($user->id);
    }

    /**
     * Userテーブルにapp_user_idの登録があるか確認
     *
     * @param object $user
     * @param boolean $uc
     * @param integer $checkCounter
     * @return bool | object
     */
    private static function checkUserWreckage(
        object $user, bool &$uc, int &$checkCounter
    ) {
        // 認証済みユーザー自体が存在するか確認
        if (!$user) {
            return false;
        }

        // app_user_idの登録がある確認
        if ($user->app_user_id !== '') {
            $uc = true;
            $checkCounter++;
            return $user;
        }
        return false;
    }

    /**
     * AppUserテーブルにデータが登録されているか確認
     *
     * @param string $name
     * @param boolean $au
     * @param integer $checkCounter
     * @return bool | object
     */
    private static function checkAppUserWreckage(
        string $name = null, bool &$au, int &$checkCounter
    ) {
        // ユーザー名が設定されているか確認
        if (!$name) {
            return '';
        }
        // AppUserを取得しデータの存在を確認
        $appuser = AppUser::getAppUserByUserName($name);
        if ($appuser) {
            $au = true;
            $checkCounter++;
            return $appuser;
        }
        return false;
    }

    /**
     * Credentialsテーブルにデータが登録されているか確認
     *
     * @param [type] $userid
     * @param boolean $cr
     * @param integer $checkCounter
     * @return bool | object
     */
    private static function checkCredentialsWreckage(
        $userid = null, bool &$cr, int &$checkCounter
    ) {
        // ユーザーIDが設定されているか確認
        if (!$userid) {
            return false;
        }
        // Credentialsを取得しデータの存在確認
        $credential = Credentials::getCredentialsById($userid);
        if ($credential) {
            $cr = true;
            $checkCounter++;
            return $credential;
        }

        return false;
    }

    /**
     * 登録済みFidoKey情報を削除
     *
     * @return bool
     */
    public static function deleteCredential(): bool
    {
        $User = Auth::user();
        if ($User->appUser) {
            // Credentials消す
            Credentials::deleteCredentialById($User->app_user_id);
            // AppUser消す
            AppUser::deleteAppUserByUserId($User->app_user_id);
            // Userのapp_user_idを消す
            User::deleteUserHash($User->id);

            // ログイン情報が残っている場合消す
            CirtificateCache::deleteCache($User->app_user_id);
            return true;
        }

        return false;
    }

    /**
     * ログインキャッシュを削除
     *
     * @return boolean
     */
    public static function deleteCirtificateCache(): bool
    {
        $User = Auth::user();
        if ($User->appUser) {
            // ログイン情報が残っている場合消す
            CirtificateCache::deleteCache($User->app_user_id);
            return true;
        }

        return false;
    }

    /**
     * 取り出したトークン情報の作成日時が
     * 設定された有効期限内の場合は返す
     * 一つも存在しない場合は[""]を返す
     *
     * @param Array $tokens
     * @return string
     */
    private function expiredChecker(Array $tokens): string
    {
        $ex = config('app.expire_time');

        foreach ($tokens as $key => $val) {
            $ch_time = new Carbon($tokens[$key]['created_at']);
            $ex_time = new Carbon();
            dump('chtime::' . $ch_time);
            dump('extime::' . $ex_time);
            dump('difftime::'.$ch_time->diffInSeconds($ex_time));
            if ($ch_time->diffInSeconds($ex_time) < $ex) {
                return $tokens[$key]['id'];
            }
        }
        return '';
    }
}
