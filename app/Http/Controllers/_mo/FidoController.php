<?php

namespace App\Http\Controllers\_mo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\_mo\Model\CirtificateCache;
use App\_mo\Model\AppUser;
use App\_mo\Model\Credentials;
use App\_mo\Model\FidoU2F;
use App\_lib\Helper\ReturnMessage;


class FidoController extends Controller
{
    
    /**
     * ログインから指定時間経過しているか判定
     *
     * @param Request $request
     * @return string True: certified False: uncirtified
     */
    public function checkExpired(Request $request)
    {
        return (FidoU2F::checkExpired()) ?
            ReturnMessage::Success('certified'):
            ReturnMessage::Error('uncirtified');
    }

    /**
     * FidoKey登録済みチェック
     *
     * @param Request $request
     * @return string True: registed False: unregisted
     */
    public function checkRegist(Request $request)
    {
        return (FidoU2F::checkRegistered()) ?
            ReturnMessage::Success('registed'):
            ReturnMessage::Error('unregisted');
    }

    /**
     * 登録済みのFidoKeyを削除する
     *
     * @return ReturnMessage [success, error]
     */
    public function deleteCredential()
    {
        return (FidoU2F::deleteCredential()) ?
            ReturnMessage::Success('deleted'):
            ReturnMessage::Error('undeleted');
    }

    /**
     * ログアウト
     *
     * @return ReturnMessage [success, error]
     */
    public function logout()
    {
        return (FidoU2F::deleteCirtificateCache()) ?
            ReturnMessage::Success('success logout'):
            ReturnMessage::Error('miss logout');
    }
}
