<?php

namespace App\Http\Controllers\_mo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CredentialController extends Controller
{
    public function index()
    {
        // セッションに転送元URL情報があれば受け取る
        $currenturl = session('current');
        return view('regist')->with('current', $currenturl);
    }
}
