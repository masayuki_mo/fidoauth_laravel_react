<?php

namespace App\Http\Controllers\_mo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;

use App\_mo\Model\AppUser;
use App\_mo\Model\Credentials;
use App\_mo\Model\RedisServer;
use App\_mo\Model\CacheUser;
use App\_mo\Model\FidoU2F;

use App\_lib\Fido\Fido;
use App\_lib\Helper\ReturnMessage;
use App\_lib\Helper\FidoHelper;

class RegisterController extends Controller
{
    private $authData;
    private $rpid   = 'yasukosan.dip.jp';
    private $rpname = 'yasukosan.desuyo';


    /**
     * 新規登録処理開始
     *
     * @param Request $request
     * @return array
     */
    public function register(Request $request): array
    {
        // ユーザー情報を全削除(デバッグ用機能)
        // AppUser::deleteAppUserAll();

        // ごみ処理後のデータを受け取る
        $AuthUser = FidoU2F::deleteMissData();

        // 作成済みのユーザーと被っていないか確認
        // app_user_id未登録、同一名のユーザーがいない場合のみOK
        //dump($AuthUser);
        if (
            $AuthUser->app_user_id != ''
            || !AppUser::checkNewUser($AuthUser->name)
        ) {
            return ReturnMessage::Error('Already Set Up User');
        }
        // Fidoヘルパー関数取得
        $cr = Fido::CredentialRepository();
        
        $clientCredentialOption = $cr
            // RP情報登録
            ->setRP([
                'id' => $this->rpid,
                'name' => $this->rpname])
            // クライアントからの情報を登録
            ->setClientRequest($this->convForm($AuthUser))
            // clientCredentailOptionを作成
            ->buildClientCredentialOptionToCreate()
            // FIDO-U2Fサポートを追加
            ->addEC2KeyType()
            // clientCredentialOptionを取得
            ->getClientCredentialOption();

        // RedisにRPIDを登録
        // $redis = new RedisServer();
        // $redis->setKeyForFIDO($cr->getHashKeys());
        // データベースにRPIDを登録
        CacheUser::addCache(
            $cr->getHashKeys()
        );

        // データベースにユーザー登録
        AppUser::addAppUser(AppUser::buildRegistData(
            $AuthUser->name,
            $cr->getHashKeys()
        ));
        
        // ハッシュキーをUserに追加
        User::addUserHash(
            $AuthUser->id,
            $cr->getHashKeys()
        );

        return $clientCredentialOption;
    }

    /**
     * 認証受付処理
     *
     * @param Request $request
     * @return string
     */
    public function registrationFinish(Request $request): array
    {

        // Credentialヘルパー関数取得
        $cr = Fido::CredentialRepository();

        // registrationId検証
        /*
        $redis = new RedisServer();
        $keys = $redis->searchKeyForFIDO($request['registrationId']);
         */
        $keys = CacheUser::searchCache($request['registrationId']);

        // ユーザーデータ取得
        // $user = AppUser::getAppUserWithCredentialsByUserId($keys[0]);
        $user = AppUser::getAppUserByUserId($keys[0]);
        if (!$user) return ReturnMessage::Error('Not User');
        
        /**
         * RPIDキャッシュの削除
         */

        CacheUser::deleteCache($user->userid);
        if (!$keys) return ReturnMessage::Error('RegistrationId Not Set');

        // ユーザーデータ取得
        $user = AppUser::getAppUserByRegistrationToken($request['registrationId']);
        if (!$user) return ReturnMessage::Error('RegistrationId Not Allow');

        // Attestationヘルパー関数取得
        $at = Fido::AttestationRepository()
                ->easySetupToRegist($request['credential'], $keys);

        // RP情報の検証
        if (
            $cr->setRP(['id' => $this->rpid, 'name' => $this->rpname])
                ->checkRP($at->callClientDataJsonRepository()->getclientData())
        ){
            // 取得した公開鍵の保存
            Credentials::addCredential(
                $at->getCredentialSaveData($user)
            );
        } else {
            return ReturnMessage::Error('Credential Failed');
        }

        return ReturnMessage::Success('Success New Regist');
    }


    private function convForm(object $user): object
    {
        return (object) array(
            'username'  => $user['name'],
        );
    }

}
