<?php

namespace App\Http\Controllers\_mo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\User;

use App\_mo\Model\AppUser;
use App\_mo\Model\Credentials;
use App\_mo\Model\RedisServer;
use App\_mo\Model\CacheUser;
use App\_mo\Model\CirtificateCache;
use App\_lib\Fido\Fido;
use App\_lib\Helper\ReturnMessage;

class AuthController extends Controller
{

    private $authData;
    private $rpid   = 'yasukosan.dip.jp';
    private $rpname = 'yasukosan.desuyo';

    /**
     * 認証開始処理
     *
     * @param Request $request
     * @return array
     */
    public function authenticate(Request $request): array
    {
        // ゴミキャッシュの削除
        CacheUser::deleteExpiredCache();
        
        $_user = Auth::user();
        
        // ユーザーの作成済みチェック
        $user = AppUser::getAppUserWithCredentialsByUsername($_user->name);
            
        if (!$user) return ReturnMessage::Error('Not User');

        // Fidoヘルパー関数取得
        $cr = Fido::CredentialRepository();

        $clientCredentialOption = $cr
            // RP情報登録
            ->setRP([
                'id' => $this->rpid,
                'name' => $this->rpname])
            // クライアントからの情報を登録
            ->setClientRequest($request)
            // clientCredentailOptionを作成
            ->buildClientCredentialOptionToGet($user)
            // clientCredentialOptionを取得
            ->getClientCredentialOption();

        // RedisにRPID、Challenge、AssertionIdを登録
        // $redis = new RedisServer();
        // $redis->setKeyForFIDO($cr->getHashKeys());
        // データベースにRPIDを登録
        CacheUser::addCache(
            $cr->getHashKeys()
        );

        return $clientCredentialOption;
    }

    /**
     * 認証受付処理
     *
     * @param Request $request
     * @return array
     */
    public function authenticateFinish(Request $request)
    {
        // AssertionId検証
        /*
        $redis = new RedisServer();
        $keys = $redis->searchKeyForFIDO($request['assertionId']);
         */

        $keys = CacheUser::searchCache($request['assertionId']);
        // 情報が無い場合はエラーを返す
        if (!$keys) return ReturnMessage::Error('Assertion ID Error');

        // ユーザーデータ取得
        $user = AppUser::getAppUserWithCredentialsByUserId($keys[0]);
        if (!$user) return ReturnMessage::Error('Not User');

        // AttestationRepository取得
        $count = Fido::AttestationRepository()
                ->easySetupToAuth($request['credential'], $user, $keys);

        if (!is_int($count)) return ReturnMessage::Error('Counter Error');

        /**
         * FidoKeyのカウンター更新
         * 何故必要なのか不明
         */
        Credentials::updateCountById($user->toArray(), (int)$count);
        
        /**
         * 認証キャッシュに登録
         */
        CirtificateCache::addCache(array('app_user_id' => $user->userid));
      
        return ReturnMessage::Success('Success Login');
        // return redirect($request['current']);
    }

    /**
     * PasswordGrantTokenの生成
     *
     * @param Request $request
     * @return void
     */
    //public function register(Request $request)
    //{
    //    /*
    //    $request->validate([
    //        'email'     => 'required|exist:users,email',
    //        'password'  => 'required'
    //    ]);
    //    */
    //    
    //    if (Auth::attempt([
    //        'email'     => $request->email,
    //        'password'  => $request->password
    //        ])
    //    ) {
    //        $user = Auth::user();

    //        $token = $user->createToken($user->email.'-'.now());

    //        return response()->json([
    //            'token'   => $token->accessToken,
    //        ]);
    //    }
    //}
}
