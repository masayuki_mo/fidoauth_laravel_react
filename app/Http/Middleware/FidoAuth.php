<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\_mo\Model\FidoU2F;

class FidoAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!FidoU2f::checkRegistered() || !FidoU2F::checkExpired()) {
            // 元の参照ページを取得し、セッションで渡す
            $currenturl = \Request::getRequestUri(); 
            return redirect('2fa')->with('current', $currenturl);
        }
        return $next($request);
    }
}
