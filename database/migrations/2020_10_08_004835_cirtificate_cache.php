<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CirtificateCache extends Migration
{
    /**
     * Fido認証済み情報の一時保存テーブル
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cirtificate_cache', function (Blueprint $table) {
            $table->string('app_user_id', 255)->comment('ユーザーID');
            $table->timestamp('expires_out')->useCurrent()->comment('有効期限');
            $table->unique('app_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cirtificate_cache');
    }
}
