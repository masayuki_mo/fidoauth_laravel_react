<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CacheUser extends Migration
{
    /**
     * CacheUserテーブル作成
     * FidoKey使用時に生成される一時データの保存用テーブル
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cache_user', function (Blueprint $table) {
            $table->string('cache_id', 255)->comment('キャッシュID');
            $table->text('cache_data')->comment('キャッシュデータ');
            $table->timestamps();
            $table->primary('cache_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cache_user');
    }
}
