<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Credentials extends Migration
{
    /**
     * FidoKey登録用テーブル
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credentials', function (Blueprint $table) {
            $table->string('id', 128);
            $table->unsignedBigInteger('app_user_id')->comment('ユーザーID');
            $table->bigInteger('count')->comment('カウンター');
            $table->text('credential_id')->comment('認証ID');
            $table->text('public_key')->comment('公開鍵');
            $table->primary('app_user_id');
            $table->unique('app_user_id');
            $table->foreign('app_user_id')->references('id')->on('app_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('credentials');
    }
}
