#!/bin/bash 

cd /var/www/html
# Install composer package
composer install
# database initialise
php artisan migrate
# laravel pasport initialise
php artisan passport:client --personal